import 'react-native';
import React, { Component } from "react";
import rendererUtil from 'react-native-test-utils'
jest.mock('react-native-fetch-blob', () => {
  return {
    DocumentDir: () => {},
    polyfill: () => {},
  }
});
jest.mock('react-native-device-info', () => {
  return {
    getUniqueID: jest.fn()
  }
});
import "firebase/storage";
import BuatLapakJual from '../containers/BuatLapakJual'
import renderer from 'react-test-renderer';
import AlertUI from 'react-native-awesome-alerts';
import ImagePicker from 'react-native-image-picker';



it('renders correctly app', () => {
    const rendered = renderer.create(<BuatLapakJual />).toJSON();
	expect(rendered).toBeTruthy();
});

test('it has the correct text for input', () => {
  let view = rendererUtil(<BuatLapakJual />)

  let textInputView = view.query("TextInput[id='title']")
  textInputView.simulate('changeText', 'Dijual ikan salmon')
  expect(view.state().title).toEqual("Dijual ikan salmon")
  textInputView = view.query("TextInput[id='price']")
  textInputView.simulate('changeText', '20000')
  expect(view.state().price).toEqual("20.000")
  textInputView = view.query("TextInput[id='weight']")
  textInputView.simulate('changeText', '200')
  expect(view.state().weight).toEqual("200")
  textInputView = view.query("TextInput[id='description']")
  textInputView.simulate('changeText', 'Ikan salmon segar mantap')
  expect(view.state().description).toEqual("Ikan salmon segar mantap")
})

test('It show the alert for todo', () => {
  jest.mock('TouchableHighlight', () => 'TouchableHighlight')
  let view = rendererUtil(<BuatLapakJual />)

  let textInputView = view.query("TouchableHighlight[id='add']")
  textInputView.simulate('press', {})
  expect(view.state().showAlertConfirm).toEqual(true)
})

test('Test Change the numeric', () =>{
  let view = rendererUtil(<BuatLapakJual />)
  let BuatFunc = renderer.create(<BuatLapakJual />).getInstance();
  expect(BuatFunc.onInputNumeric("asdqwe12412414")).toEqual("12.412.414")
  expect(BuatFunc.onInputNumeric("10000010")).toEqual("10.000.010")
  expect(BuatFunc.onInputNumeric("1020 102 0.12da")).toEqual("1.020.102.012")
  expect(BuatFunc.changeToNumeric("1.020.102.012")).toEqual(1020102012)
})

test('Test Validate From', () =>{
  let view = rendererUtil(<BuatLapakJual />)
  let BuatFunc = renderer.create(<BuatLapakJual />).getInstance();
  expect(BuatFunc.validateForm({
    title: '',
    imageSource1: null,
    jenisIkan: '',
    olahan: '',
    namaIkan: 'Pilih Nama Ikan',
    price: '',
    ukuranIkan: '',
    weight: '',
    description: '',
    date: date
  })).toEqual("Isi judul dari ikan \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: null,
    jenisIkan: '',
    olahan: '',
    namaIkan: 'Pilih Nama Ikan',
    price: '',
    ukuranIkan: '',
    weight: '',
    description: '',
    date: ''
  })).toEqual("Pilih minimal 1 gambar \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: '',
    olahan: '',
    namaIkan: 'Pilih Nama Ikan',
    price: '',
    ukuranIkan: '',
    weight: '',
    description: '',
    date: ''
  })).toEqual("Isi jenis pengolahan produk \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Olahan',
    olahan: '',
    namaIkan: 'Pilih Nama Ikan',
    price: '',
    ukuranIkan: '',
    weight: '',
    description: '',
    date: ''
  })).toEqual("Isi cara pengolahan ikan \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Segar',
    olahan: '',
    namaIkan: 'tuna',
    price: '',
    ukuranIkan: '',
    weight: '',
    description: '',
    date: ''
  })).toEqual("Isi jumlah/bobot dari ikan \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Segar',
    olahan: '',
    namaIkan: 'tuna',
    price: '',
    ukuranIkan: '',
    weight: '15',
    description: '',
    date: ''
  })).toEqual("Isi harga dari ikan \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Segar',
    olahan: '',
    namaIkan: 'tuna',
    price: '10.000',
    ukuranIkan: '',
    weight: '15',
    description: '',
    date: ''
  })).toEqual("Isi ukuran dari ikan \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Segar',
    olahan: '',
    namaIkan: 'tuna',
    price: '10.000',
    ukuranIkan: 'kecil',
    weight: '12',
    description: '',
    date: ''
  })).toEqual("Isi deskripsi dari ikan \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Segar',
    olahan: '',
    namaIkan: 'tuna',
    price: '10.000',
    ukuranIkan: 'kecil',
    weight: '12',
    description: 'test',
    date: ''
  })).toEqual("Isi tanggal ikan ditangkap \n")

  expect(BuatFunc.validateForm({
    title: 'Dijual ikan salmon',
    imageSource1: "null",
    jenisIkan: 'Segar',
    olahan: '',
    namaIkan: 'tuna',
    price: '10.000',
    ukuranIkan: 'kecil',
    weight: '12',
    description: 'test',
    date: "2018-01-01, 20:11"
  })).toEqual("")
})

