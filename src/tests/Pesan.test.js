import React from "react";
import { Text, View, StyleSheet, ListView, TextInput, Button } from "react-native";
import { List, ListItem } from "react-native-elements";
import Pesan from "../containers/Pesan";

import renderer from "react-test-renderer";

it("renders Text without crashing", () => {
	const rendered = renderer.create(<Text />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders View without crashing", () => {
	const rendered = renderer.create(<View />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders TextInput without crashing", () => {
	const rendered = renderer.create(<TextInput />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders List without crashing", () => {
	const rendered = renderer.create(<List />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders ListItem without crashing", () => {
	const rendered = renderer.create(<ListItem />).toJSON();
	expect(rendered).toBeTruthy();
});

jest.mock("react-native-device-info", () => {
	return {
		getUniqueID: jest.fn()
	};
});