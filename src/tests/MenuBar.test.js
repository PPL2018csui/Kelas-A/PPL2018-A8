import "firebase/storage";
import 'react-native';
import React, { Component } from "react";
import MenuBar from "../containers/MenuBar";
import Beranda from "../containers/Beranda";
import Lapak from "../containers/Lapak";
import Pesan from "../containers/DaftarPesan";
import Profile from "../containers/Profile";
jest.mock('react-native-fetch-blob', () => {
  return {
    DocumentDir: () => {},
    polyfill: () => {},
  }
});
jest.mock('react-native-device-info', () => {
  return {
    getUniqueID: jest.fn()
  }
});
import BuatLapakJual from '../containers/BuatLapakJual'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';


it("renders correctly app", () => {
	const rendered = renderer.create(<MenuBar />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders correctly Beranda", () => {
	const navigation = { navigate: jest.fn() };
	const rendered = renderer.create(<Beranda />).toJSON();
	expect(rendered).toBeTruthy();
	navigation.navigate("Beranda");
});

// it("renders correctly Lapak", () => {
// 	const rendered = renderer.create(<Lapak />).toJSON();
// 	expect(rendered).toBeTruthy();
// });

it("renders correctly Pesan", () => {
	const rendered = renderer.create(<Pesan />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders correctly Profile", () => {
	const rendered = renderer.create(<Profile />).toJSON();
	expect(rendered).toBeTruthy();
});