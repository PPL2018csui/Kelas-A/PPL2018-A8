import React from 'react';
import { HeaderDetails, ProductInfo, ProductDesc, SellerInfo } from '../components/Components'
import renderer from 'react-test-renderer';

describe('rendering <ProductDetails> components', () => {
	it("renders HeaderDetails without crashing", () => {
		const rendered = renderer.create(<HeaderDetails />).toJSON();
		expect(rendered).toBeTruthy();
	});
	it("renders ProductInfo without crashing", () => {
		const rendered = renderer.create(<ProductInfo />).toJSON();
		expect(rendered).toBeTruthy();
	});
	it("renders ProductDesc without crashing", () => {
		const rendered = renderer.create(<ProductDesc />).toJSON();
		expect(rendered).toBeTruthy();
	});
	it("renders SellerInfo without crashing", () => {
		const rendered = renderer.create(<SellerInfo />).toJSON();
		expect(rendered).toBeTruthy();
	});
})