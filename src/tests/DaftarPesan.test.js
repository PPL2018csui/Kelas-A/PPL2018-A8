import React from "react";
import { Text, View, ScrollView } from "react-native";
import DaftarPesan from "../containers/DaftarPesan";

import renderer from "react-test-renderer";

it("renders Text without crashing", () => {
	const rendered = renderer.create(<Text />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders View without crashing", () => {
	const rendered = renderer.create(<View />).toJSON();
	expect(rendered).toBeTruthy();
});

it("renders ScrollView without crashing", () => {
	const rendered = renderer.create(<ScrollView />).toJSON();
	expect(rendered).toBeTruthy();
});

jest.mock("react-native-device-info", () => {
	return {
		getUniqueID: jest.fn()
	};
});
