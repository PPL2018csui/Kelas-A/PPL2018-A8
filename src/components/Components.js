import React, { Component } from 'react';
import { AppRegistry, View, Text, Image, ScrollView, StyleSheet, TouchableHighlight } from 'react-native';
import { headerDetailsStyles, productInfoStyles, productDescStyles, sellerInfoStylesheet } from '../assets/stylesheet/productDetailsStyle';
import { Icon } from 'react-native-elements';
import Timestamp from 'react-timestamp';
import ActionButton from 'react-native-action-button';

export class HeaderDetails extends Component {
    render() {
        return (
            <View style={headerDetailsStyles.mainContainer}>
                <View style={headerDetailsStyles.imgContainer}>
                    <Image
                        style={headerDetailsStyles.image}
                        source={{ uri: this.props.imgSource }} />
                </View>
                <View style={headerDetailsStyles.container}>
                    <Text style={headerDetailsStyles.headTitle}>
                        {this.props.title}
                    </Text>
                </View>
                <View style={headerDetailsStyles.container}>
                    <Text style={headerDetailsStyles.price}>
                        Rp {this.props.price} /{this.props.satuan}
                    </Text>
                </View>
                <View style={headerDetailsStyles.section}>
                    <View style={headerDetailsStyles.separator} />
                </View>
                <View style={headerDetailsStyles.section}>
                    <View style={headerDetailsStyles.details}>
                        <View style={headerDetailsStyles.icon}>
                            <Icon name='location-pin' type='entypo' style={headerDetailsStyles.actionButtonIcon} />
                        </View>
                        <Text style={headerDetailsStyles.contentDetail}>
                            {this.props.location}
                        </Text>
                    </View>
                    <View style={headerDetailsStyles.details}>
                        <View style={headerDetailsStyles.icon}>
                            <Icon name='md-calendar' type='ionicon' style={headerDetailsStyles.actionButtonIcon} />
                        </View>
                        <Text style={headerDetailsStyles.contentDetail}>
                            <Timestamp component={Text} format='full' time={this.props.timestamp} precision={3} />
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

export class ProductInfo extends Component {
    render() {
        return (
            <View style={productInfoStyles.mainContainer}>
                <Text style={productInfoStyles.subTitle}>
                    Informasi Produk
                </Text>
                <View style={productInfoStyles.section}>
                    <View style={productInfoStyles.separator} />
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Ukuran
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.ukuran}
                    </Text>
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Jumlah
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.weight} {this.props.satuan}
                    </Text>
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Kondisi Ikan
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.kondisi}
                    </Text>
                </View>
            </View>
        )
    }
}

export class ProductDesc extends Component {
    render() {
        return (
            <View style={productDescStyles.mainContainer
            }>
                <Text style={productDescStyles.subTitle}>
                    Deksripsi Produk
                </Text>
                <View style={productDescStyles.section}>
                    <View style={productDescStyles.separator}
                    />
                </View>
                <Text style={productDescStyles.content}>
                    {this.props.description}
                </Text>
            </View>
        )
    }
}

export class SellerInfo extends Component {
    render() {
        return (
            <View style={sellerInfoStylesheet.mainContainer}>
                <Text style={sellerInfoStylesheet.subTitle}>
                    Penjual
                </Text>
                <View style={sellerInfoStylesheet.section}>
                    <View style={sellerInfoStylesheet.separator}
                    />
                </View>
                <View style={sellerInfoStylesheet.details}>
                    <View style={sellerInfoStylesheet.space}>
                        <Icon
                            name='user'
                            type='feather'
                            color='#999999'
                            size={24}
                        />
                    </View>
                    <View style={sellerInfoStylesheet.box}>
                        <Text style={sellerInfoStylesheet.content}>
                            {this.props.seller}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
