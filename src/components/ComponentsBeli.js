import React, { Component } from 'react';
import { AppRegistry, View, Text, Image, ScrollView, StyleSheet, TouchableHighlight } from 'react-native';
import { headerDetailsStyles, productInfoStyles, productDescStyles, sellerInfoStylesheet } from '../assets/stylesheet/productDetailsStyle';
import { Icon } from 'react-native-elements';
import Timestamp from 'react-timestamp';
import ActionButton from 'react-native-action-button';



export class HeaderDetails extends Component {
    render() {
        return (
            <View style={headerDetailsStyles.mainContainer}>
                <View style={headerDetailsStyles.container}>
                    <Text style={headerDetailsStyles.headTitle}>
                        {this.props.title}
                    </Text>
                </View>
                <View style={headerDetailsStyles.container}>
                    <Text style={headerDetailsStyles.price}>
                        Rp {this.props.price} /{this.props.satuan}
                    </Text>
                </View>
                <View style={headerDetailsStyles.section}>
                    <View style={headerDetailsStyles.separator} />
                </View>
                <View style={headerDetailsStyles.section}>
                    <View style={headerDetailsStyles.details}>
                        <View style={headerDetailsStyles.icon}>
                            <Icon name='location-pin' type='entypo' style={headerDetailsStyles.actionButtonIcon} />
                        </View>
                        <Text style={headerDetailsStyles.contentDetail}>
                            {this.props.location}
                        </Text>
                    </View>
                    <View style={headerDetailsStyles.details}>
                        <View style={headerDetailsStyles.icon}>
                            <Icon name='md-calendar' type='ionicon' style={headerDetailsStyles.actionButtonIcon} />
                        </View>
                        <Text style={headerDetailsStyles.contentDetail}>
                            <Timestamp component={Text} format='full' time={this.props.timestamp} precision={3} />
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}

export class ProductInfo extends Component {
    render() {
        return (
            <View style={productInfoStyles.mainContainer}>
                <Text style={productInfoStyles.subTitle}>
                    Informasi
                </Text>
                <View style={productInfoStyles.section}>
                    <View style={productInfoStyles.separator} />
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Jumlah
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.weight} {this.props.satuan}
                    </Text>
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Kondisi Ikan
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.kondisi}
                    </Text>
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Pembayaran
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.pembayaran}
                    </Text>
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Nama Ikan
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                        {this.props.namaIkan.substring(0,1).toUpperCase()}{this.props.namaIkan.slice(1)}
                    </Text>
                </View>
                <View style={productInfoStyles.details}>
                    <Text style={productInfoStyles.contentInfo}>
                        Harga Total
                    </Text>
                    <Text style={productInfoStyles.contentDetail}>
                    Rp {this.props.totalHarga} 
                    </Text>
                </View>
            </View>
        )
    }
}

export class SellerInfo extends Component {
    render() {
        return (
            <View style={sellerInfoStylesheet.mainContainer}>
                <Text style={sellerInfoStylesheet.subTitle}>
                    Pembeli
                </Text>
                <View style={sellerInfoStylesheet.section}>
                    <View style={sellerInfoStylesheet.separator}
                    />
                </View>
                <View style={sellerInfoStylesheet.details}>
                    <View style={sellerInfoStylesheet.space}>
                        <Icon
                            name='user'
                            type='feather'
                            color='#999999'
                            size={24}
                        />
                    </View>
                    <View style={sellerInfoStylesheet.box}>
                        <Text style={sellerInfoStylesheet.content}>
                            {this.props.seller}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
