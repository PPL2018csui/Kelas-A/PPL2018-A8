import * as firebase from "firebase";

const config = {
	apiKey: "AIzaSyDuIquf4EeA-unVF4VpKAvUKisL3vc2xM8",
	authDomain: "ppla8-5cd59.firebaseapp.com",
	databaseURL: "https://ppla8-5cd59.firebaseio.com",
	projectId: "ppla8-5cd59",
	storageBucket: "ppla8-5cd59.appspot.com",
	messagingSenderId: "994468122087"
};
firebase.initializeApp(config);

export default firebase;