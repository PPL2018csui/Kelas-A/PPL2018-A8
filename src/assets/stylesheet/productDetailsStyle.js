import { StyleSheet } from 'react-native';

export const headerDetailsStyles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        backgroundColor: 'white',
    },
    headTitle: {
        color: '#333333',
        fontSize: 36,
        fontFamily: 'Montserrat-Bold',
    },
    imgContainer: {
        height: 300,
        alignItems: 'center',
    },
    image: {
        flex: 1,
        height: 300,
        width: 300,
        resizeMode: 'contain'
    },
    price: {
        color: 'red',
        fontSize: 36,
        fontFamily: 'Montserrat-Regular',
    },
    contentDetail: {
        fontSize: 16,
        color: '#999999',
        fontFamily: 'Montserrat-Regular',
        flex: 0.9,
    },
    separator: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    details: {
        flexDirection: 'row',
        flex: 1,
    },
    icon: {
        flex: 0.1,
    },
    section: {
        paddingVertical: 5,
        backgroundColor: 'white',
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
});

export const productInfoStyles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        paddingBottom: 5,
    },
    subTitle: {
        color: '#333333',
        fontSize: 28,
        fontFamily: 'Montserrat-Regular',
    },
    image: {
        flex: 1,
        height: 300,
        width: 300,
        resizeMode: 'contain'
    },
    contentInfo: {
        fontSize: 16,
        color: '#999999',
        fontFamily: 'Montserrat-Regular',
        flex: 0.4,
    },
    contentDetail: {
        fontSize: 16,
        color: '#999999',
        fontFamily: 'Montserrat-Regular',
        flex: 0.6,
    },
    separator: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    details: {
        flexDirection: 'row',
        flex: 1,
    },
    section: {
        paddingVertical: 5,
        backgroundColor: 'white',
    },
});

export const productDescStyles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        paddingBottom: 5,
    },
    subTitle: {
        color: '#333333',
        fontSize: 28,
        fontFamily: 'Montserrat-Regular',
    },
    content: {
        fontSize: 16,
        color: '#999999',
        fontFamily: 'Montserrat-Regular',
        paddingBottom: 5,
    },
    separator: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    details: {
        flexDirection: 'row',
        flex: 1,
    },
    section: {
        paddingVertical: 5,
        backgroundColor: 'white',
    },
});

export const sellerInfoStylesheet = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginBottom: 10,
        paddingHorizontal: 10,
        backgroundColor: 'white',
        paddingBottom: 5,
    },
    subTitle: {
        color: '#333333',
        fontSize: 28,
        fontFamily: 'Montserrat-Regular',
    },
    content: {
        fontSize: 16,
        color: '#999999',
        fontFamily: 'Montserrat-Regular',
    },
    separator: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    details: {
        flexDirection: 'row',
        flex: 1,
    },
    section: {
        paddingVertical: 5,
        backgroundColor: 'white',
    },
    space: {
        flex: 0.2,
    },
    box: {
        flex: 0.7,
        paddingBottom: 5,
        justifyContent: 'center',
    },
    chatButton: {
        alignItems: 'center',
        backgroundColor: '#1e90ff',
        width: 100,
        height: 20,
        borderRadius: 10
    },
    buttonText: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
        color: 'white',
        flex: 1,
    },
    button: {
        flex: 0.3,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export const searchBarStylesheet = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    container: {
        flex: 0.1
    },
    content: {
        color: '#999999',
        fontFamily: 'Helvetica-Regular',
        fontSize: 14,
        textAlignVertical: 'center',
        flex: 0.9,
    },
    background: {
        backgroundColor: '#1e90ff',
        alignItems: 'center',
        flexDirection: 'row',
        flex: 0.1,
    },
    subContainer: {
        borderRadius: 25,
        margin: 10,
        padding: 5,
        backgroundColor: '#ffffff',
        flex: 1,
        height: 45,
        alignItems: 'center',
    },
});