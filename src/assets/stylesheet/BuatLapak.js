import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

export const buatLapakBeliStyles = StyleSheet.create({
container: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "center",
		backgroundColor: "#f0f8ff"
	},
	content: {
		marginTop: 5,
		marginBottom: 5,
		paddingLeft : 10,
		paddingRight : 10
	},
	contentText: {
		fontSize: 18,
		fontFamily: "Montserrat-Regular",
		color: "#666666"
	},
	contentModal: {
		marginTop: 5,
		marginBottom: 5,
		paddingLeft : 10,
		paddingRight : 10
	},
	contentTextModal: {
		fontSize: 15,
		fontFamily: "Montserrat-Regular",
		color: "#666666"
	},
	textHeader: {
		fontSize: 36,
		color: "white",
		fontWeight: "bold",
		textAlign: "left",
		alignSelf: "stretch"
	},
	textFooter: {
		fontSize: 20,
		color: "white",
		fontFamily: "Helvetica-Bold"
		
	},
	imageContainer: {
		flex: 1
	},
	image: {
		width: 100,
		height: 100,
	},
	footer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#1e90ff'
	  },
	buttonFooter: {
		alignItems: 'center',
		padding: 10
	  },
	picker2: {
		flex: 1,
		flexDirection: 'row'
	},
	modal: {
    justifyContent: 'center',
		alignItems: 'center',
		height: 300,
    width: 300
  },
	btn: {
    margin: 10,
    backgroundColor: "#1e90ff",
    padding: 10,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 15,
    fontFamily: "Montserrat-Regular",
    color: "white"
  },
  alertUIText: {
    fontSize:15, 
    fontFamily:"Montserrat-Regular"
  },
	containerLoading: {
		justifyContent: 'center'
	},
	pickerListIkan: {
		marginTop: 15,
		marginBottom: 15
	},
	pickerTextListIkan: {
		fontSize: 15,
		fontFamily:'sans-serif-light',
		color: '#666666'
	},
	overlayStyleListIkan:{
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		backgroundColor: '#333333',
		justifyContent: 'center',
		alignItems: 'center'
	},
	listContainerStyleListIkan: {
		flex: 1,
		width: width * 0.8,
		maxHeight: height * 0.7,
		backgroundColor: '#fff',
		borderRadius: 10,
		marginBottom: 15,
		overflow: 'hidden'
	},
	optionTextStyleListIkan: {
		fontSize:15, 
		fontFamily:"Montserrat-Regular"
	},
	filterTextInputStyleListIkan: {
		paddingVertical: 10,
		paddingHorizontal: 15,
		flex: 0,
		height: 50,
		fontFamily: "Montserrat-Regular",
		fontSize:15
	},
	titleTextStyleListIkan: {
		fontFamily:"Helvetica-Bold", 
		color:"white", 
		fontSize:20,
		flex: 0,
		marginBottom: 15
	},
	noResultsText: {
    flex: 1,
    textAlign: 'center',
    color: '#ccc',
    fontStyle: 'italic',
    fontSize: 22
  }
});
