import React, { Component } from 'react';
import { AppRegistry, View, Text, Button, Alert, Image, ScrollView, TouchableHighlight } from 'react-native';
import * as firebase from 'firebase';
import ProductDetails from "./ProductDetails";
import { StackNavigator } from 'react-navigation';

const firebaseConfig = {
    apiKey: "AIzaSyDuIquf4EeA-unVF4VpKAvUKisL3vc2xM8",
    authDomain: "ppla8-5cd59.firebaseapp.com",
    databaseURL: "https://ppla8-5cd59.firebaseio.com/",
    storageBucket: "gs://ppla8-5cd59.appspot.com",
}

const Timestamp = require('react-timestamp');
const fbRef = firebase.initializeApp(firebaseConfig);

export default class Layout extends Component {
  render() {
    var dummy = {
			'title': 'IKAN TONGKOL',
			'timestamp': '15500022',
			'weight': 33,
			'price': 15000,
			'loc': 'Jakarta',
			'desc': 'Ikan tongkol segar',
      'seller': 'Joni',
      'source': '../images/tongkol.jpg'
		}
    const { title, timestamp, weight, price, loc, desc, seller } = dummy;
    return (
      <ScrollView style={{flex: 1, padding: 10, backgroundColor: 'aqua'}}>
        <View style={{height: 200, alignItems: 'center'}}>
          <Image style={{flex: 1, resizeMode: 'contain', margin: 10}}
            source={require({source})} />
        </View>
        <Text style={{ color:'black', fontSize: 36, backgroundColor: 'white', marginVertical: 10}}>
          IKAN TONGKOL
        </Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10}}>
          <View style={{backgroundColor: 'white'}}>
            <Text style={{ color:'black',fontSize: 24}}>
              {weight} kg
            </Text>
            <Text style={{ color:'red',fontSize: 24}}>
              Rp.{price} /kg
            </Text>
          </View>
          <View style={{backgroundColor: 'white'}}>
            <Text style={{ color:'black',fontSize: 16}}>
              dipasang pada:
            </Text>
            <Timestamp component={Text} format='full' time={timestamp} precision={3} />
            <Text style={{ color:'black',fontSize: 16}}>
              Lokasi: {loc}
            </Text>
          </View>
        </View>
        <View style={{height: 150, marginBottom: 10, backgroundColor: 'white'}}>
          <Text style={{ color:'black',fontSize: 20}}>
            Deskripsi
          </Text>
          <Text style={{ color:'black',fontSize: 16, textAlign: 'justify'}}>
            {desc}
          </Text>
        </View>
        <View style={{height: 50, flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center', backgroundColor: 'white', borderColor: 'black', borderWidth: 1}}>
          <TouchableHighlight onPress={() => Alert.alert("GG")}>
            <Text style={{flex: 1, fontSize: 20}}>{seller}</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
};

//import React, {Component} from "react";
//import { StyleSheet, Text, View } from "react-native";
//import Testing from "./Testing";
//import Layout from "./Layout";

//const App = () => {
//    return (
//        <Layout />
//    )
//}
//export default App