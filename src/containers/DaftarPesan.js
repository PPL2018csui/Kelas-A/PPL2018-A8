import React, { Component } from "react";
import { Text, StyleSheet, View, ScrollView, ListView, TouchableHighlight, FlatList, Alert } from "react-native";
import PesanItem from "./PesanItem";
import Pesan from "./Pesan";
import firebase from "../firebase.js";
import { StackNavigator } from "react-navigation";

var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class PesanScreen extends Component {
	static navigationOptions = {
		title: 'Pesan',
		headerStyle: {
			backgroundColor: '#1e90ff',
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontWeight: "normal",
			fontSize: 20,
			fontFamily: 'Helvetica-Bold'
		},
	};

	constructor(props) {
		super(props);
		this.chatRef = firebase.database().ref().child("chats/" + uniqueId);
		this.state = {
			listViewData: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 != row2}),
		};
		this.chatList = [];
	}

	componentDidMount() {
		this.chatRef.on("child_added", (dataSnapshot) => {
			var item = {};
			item.name = dataSnapshot.child("name").val();
			item.id = dataSnapshot.key;
			item.image = require("../assets/images/profpic.jpg");
			item.read = dataSnapshot.child("read").val();
			item.message_content = dataSnapshot.child("lastChat").val();
			item.lastChatTime = dataSnapshot.child("lastChatTime").val();
			this.chatList.push(item);
			this.setState({
				listViewData: this.state.listViewData.cloneWithRows(this.chatList)
			});
			this.chatList = this.chatList.sort(function(a,b) {return (b.lastChatTime - a.lastChatTime) });
		});

		this.chatRef.on("child_changed", (dataSnapshot) => {
			this.chatList = this.chatList.filter((x) => x.id != dataSnapshot.key);
			var item = {};
			item.name = dataSnapshot.child("name").val();
			item.id = dataSnapshot.key;
			item.image = require("../assets/images/profpic.jpg");
			item.read = dataSnapshot.child("read").val();
			item.message_content = dataSnapshot.child("lastChat").val();
			item.lastChatTime = dataSnapshot.child("lastChatTime").val();
			this.chatList.push(item);
			this.setState({
				listViewData: this.state.listViewData.cloneWithRows(this.chatList)
			});
			this.chatList = this.chatList.sort(function(a,b) {return (b.lastChatTime - a.lastChatTime) });
		});

		this.chatRef.on("child_removed", (dataSnapshot) => {
			this.chatList = this.chatList.filter((x) => x.id != dataSnapshot.key);
			this.setState({
				listViewData: this.state.listViewData.cloneWithRows(this.chatList)
			});
			this.chatList = this.chatList.sort(function(a,b) {return (b.lastChatTime - a.lastChatTime) });
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<FlatList
					style={styles.pesan_container}
					data={this.chatList}
					keyExtractor={(item, index) => item.id}
					renderItem={({item, index}) => (
						<TouchableHighlight 
							underlayColor="#D3D3D3"
							onPress={() => {
								this.props.navigation.navigate("RuangPesan", {
									friendId: item.id,
								});
							}}
							style={styles.button}>
							<PesanItem key={index} index={index} pesan={item} style={styles.image} />
						</TouchableHighlight>
					)}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#F0F8FF"
	},
	pesan_container: {
		flex: 1,
		flexDirection: "column",
		marginRight:10,
		marginLeft:10
	}
});
