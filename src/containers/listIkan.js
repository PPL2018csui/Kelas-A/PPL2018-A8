const listIkan = [
    {
          key: 'Albakora',
          label: 'Albakora',
      },
      {
          key: 'Alu-alu',
          label: 'Alu-alu',
      },
      {
          key: 'Bakso Ikan Tuna',
          label: 'Bakso Ikan Tuna',
      },
      {
          key: 'Bambangan',
          label: 'Bambangan',
      },
      {
          key: 'Bandeng',
          label: 'Bandeng',
      },
      {
          key: 'Banyar',
          label: 'Banyar',
      },
      {
          key: 'Baronang',
          label: 'Baronang',
      },
      {
          key: 'Bawal Hitam',
          label: 'Bawal Hitam',
      },
      {
          key: 'Bawal Putih',
          label: 'Bawal Putih',
      },
      {
          key: 'Belanak',
          label: 'Belanak',
      },
      {
          key: 'Beloso',
          label: 'Beloso',
      },
      {
          key: 'Bentong',
          label: 'Bentong',
      },
      {
          key: 'Betutu',
          label: 'Betutu',
      },
      {
          key: 'Biji Nangka',
          label: 'Biji Nangka',
      },
      {
          key: 'Bulu Ayam Asin',
          label: 'Bulu Ayam Asin',
      },
      {
          key: 'Cakalang',
          label: 'Cakalang',
      },
      {
          key: 'Cakalang Asap',
          label: 'Cakalang Asap',
      },
      {
          key: 'Cendro',
          label: 'Cendro',
      },
      {
          key: 'Cucut',
          label: 'Cucut',
      },
      {
          key: 'Cumi-cumi',
          label: 'Cumi-cumi',
      },
      {
          key: 'Cumi-cumi asin',
          label: 'Cumi-cumi asin',
      },
      {
          key: 'Ekor Kuning',
          label: 'Ekor Kuning',
      },
      {
          key: 'Gabus',
          label: 'Gabus',
      },
      {
          key: 'Gerot-gerot',
          label: 'Gerot-gerot',
      },
      {
          key: 'Golok-golok',
          label: 'Golok-golok',
      },
      {
          key: 'Gulamah/Tigawaja',
          label: 'Gulamah/Tigawaja',
      },
      {
          key: 'Gurami',
          label: 'Gurami',
      },
      {
          key: 'Gurita',
          label: 'Gurita',
      },
      {
          key: 'Hiu',
          label: 'Hiu',
      },
      {
          key: 'Ikan Mas',
          label: 'Ikan Mas',
      },
      {
          key: 'Ikan Pedang',
          label: 'Ikan Pedang',
      },
      {
          key: 'Ikan Sebelah',
          label: 'Ikan Sebelah',
      },
      {
          key: 'Japuh',
          label: 'Japuh',
      },
      {
          key: 'Jelawat',
          label: 'Jelawat',
      },
      {
          key: 'Julung-julung',
          label: 'Julung-julung',
      },
      {
          key: 'Kacangan',
          label: 'Kacangan',
      },
      {
          key: 'Kakap Merah',
          label: 'Kakap Merah',
      },
      {
          key: 'Kakap Putih',
          label: 'Kakap Putih',
      },
      {
          key: 'Kambing-kambing',
          label: 'Kambing-kambing',
      },
      {
          key: 'Kapas-kapas',
          label: 'Kapas-kapas',
    },
    {
          key: 'Kembung',
          label: 'Kembung',
      },
      {
          key: 'Kembung Asin',
          label: 'Kembung Asin',
      },
      {
          key: 'Kepiting',
          label: 'Kepiting',
      },
      {
          key: 'Kerang Darah',
          label: 'Kerang Darah',
    },
    {
          key: 'Kerang Hijau',
          label: 'Kerang Hijau',
      },
      {
          key: 'Kerapu',
          label: 'Kerapu',
      },
      {
          key: 'Kerong-kerong',
          label: 'Kerong-kerong',
      },
      {
          key: 'KuKurisi',
          label: 'KuKurisi',
    },
    {
          key: 'Kuro/Senangin',
          label: 'Kuro/Senangin',
      },
      {
          key: 'Kuwe',
          label: 'Kuwe',
      },
      {
          key: 'Layang',
          label: 'Layang',
      },
      {
          key: 'Layaran',
          label: 'Layaran',
    },
    {
          key: 'Layur',
          label: 'Layur',
      },
      {
          key: 'Lele',
          label: 'Lele',
      },
      {
          key: 'Lemadang',
          label: 'Lemadang',
      },
      {
          key: 'Lemuru',
          label: 'Lemuru',
    },
    {
          key: 'Lencam',
          label: 'Lencam',
      },
      {
          key: 'Lidah',
          label: 'Lidah',
      },
      {
          key: 'Lisong',
          label: 'Lisong',
      },
      {
          key: 'Lobster',
          label: 'Lobster',
    },
    {
          key: 'Lolosi Biru',
          label: 'Lolosi Biru',
      },
      {
          key: 'Madidihang',
          label: 'Madidihang',
      },
      {
          key: 'Manyung',
          label: 'Manyung',
      },
      {
          key: 'Marlin',
          label: 'Meka',
    },
    {
          key: 'Mujair',
          label: 'Mujair',
      },
      {
          key: 'Nila',
          label: 'Nila',
      },
      {
          key: 'Nilem',
          label: 'Nilem',
      },
      {
          key: 'Pari',
          label: 'Pari',
    },
    {
          key: 'Patin',
          label: 'Patin',
      },
      {
          key: 'Petek/Peperek',
          label: 'Petek/Peperek',
      },
      {
          key: 'Pisang-pisang',
          label: 'Pisang-pisang',
      },
      {
          key: 'Rajungan',
          label: 'Rajungan',
    },
    {
          key: 'Rajungan Kaleng',
          label: 'Rajungan Kaleng',
      },
      {
          key: 'Remang',
          label: 'Remang',
      },
      {
          key: 'Remis',
          label: 'Remis',
      },
      {
          key: 'Rumput Laut E. Cottoni',
          label: 'Rumput Laut E. Cottoni',
    },
    {
          key: 'Rumput Laut Gracia',
          label: 'Rumput Laut Gracia',
      },
      {
          key: 'Samge',
          label: 'Samge',
      },
      {
          key: 'Sardine',
          label: 'Sardine',
      },
      {
          key: 'Sawo/Kakap Batu',
          label: 'Sawo/Kakap Batu',
    },
    {
          key: 'Selanget',
          label: 'Selanget',
      },
      {
          key: 'Selar',
          label: 'Selar',
      },
      {
          key: 'Selengseng',
          label: 'Selengseng',
      },
      {
          key: 'Setuhuk Biru',
          label: 'Setuhuk Biru',
    },
    {
          key: 'Setuhuk Hitam',
          label: 'Setuhuk Hitam',
      },
      {
          key: 'Setuhuk Loreng',
          label: 'Setuhuk Loreng',
      },
      {
          key: 'Sidat',
          label: 'Sidat',
      },
      {
          key: 'Simping',
          label: 'Simping',
    },
    {
          key: 'Siro',
          label: 'Siro',
      },
      {
          key: 'Sotong',
          label: 'Sotong',
    },
    {
          key: 'Sunglir',
          label: 'Sunglir',
      },
      {
          key: 'Swangi/Mata Besar',
          label: 'Swangi/Mata Besar',
      },
      {
          key: 'Talang-talang',
          label: 'Talang-talang',
      },
      {
          key: 'Tawes',
          label: 'Tawes',
    },
    {
          key: 'Tembang',
          label: 'Tembang',
      },
      {
          key: 'Tenggiri Bulat',
          label: 'Tenggiri Bulat',
    },
    {
          key: 'Terbang',
          label: 'Terbang',
      },
      {
          key: 'Teri',
          label: 'Teri',
      },
      {
          key: 'Teri Asin',
          label: 'Teri Asin',
      },
      {
          key: 'Teripang',
          label: 'Teripang',
    },
    {
          key: 'Terubuk',
          label: 'Terubuk',
    },
    {
          key: 'Tetengkek',
          label: 'Tetengkek',
    },
    {
          key: 'Tiram',
          label: 'Tiram',
    },
    {
          key: 'Toman',
          label: 'Toman',
    },
    {
          key: 'Tongkol',
          label: 'Tongkol',
    },
    {
          key: 'Tuna Mata Besar',
          label: 'Tuna Mata Besar',
    },
    {
          key: 'Tuna Sirip Biru Selatan',
          label: 'Tuna Sirip Biru Selatan',
    },
    {
          key: 'Ubur-ubur',
          label: 'Ubur-ubr',
    },
    {
          key: 'Udang Barong',
          label: 'Udang Barong',
      },
  
    {
          key: 'Udang Dogol',
          label: 'Udang Dogol',
    },
    {
          key: 'Udang Galah',
          label: 'Udang Galah',
    },
    {
          key: 'Udang Jerbung',
          label: 'Udang Jerbung',
    },
    {
          key: 'Udang Krosok',
          label: 'Udang Krosok',
      },
    {
          key: 'Udang Putih',
          label: 'Udang Putih',
    },
    {
          key: 'Udang Putih/Vaname',
          label: 'Udang Putih/Vaname',
    },
    {
          key: 'Udang Windu',
          label: 'Udang Windu',
      },
  ]
  export default listIkan
  