import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, TextInput, Image, Button, Alert, TouchableOpacity, Picker, TouchableHighlight, ActivityIndicator, BackHandler, Dimensions } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import DatePicker from 'react-native-datepicker'
import { StackNavigator, NavigationActions } from 'react-navigation';
import AlertUI from 'react-native-awesome-alerts';
import firebase from '../firebase.js'
import Modal from "react-native-modalbox";
import Spinner from 'react-native-loading-spinner-overlay';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import { AndroidBackHandler } from 'react-navigation-backhandler';
import listIkan from './listIkan'
const { width, height } = Dimensions.get('window')
var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();
var storageRef = firebase.storage().ref();

export default class EditProdukJual extends Component {
    render() {

        return (
            <View style={styles.container}>
                    <View style={{ flex: 10 }}>
                        <ScrollView>
                            <View style={[styles.content]}>
                                <Text style={[styles.contentText]}>Judul Iklan</Text>
                                <TextInput
                                    id="title"
                                    defaultValue={this.state.title}
                                    style={{ fontFamily: "Montserrat-Regular", color: "#666666" }}
                                    onChangeText={text => this.setState({ title: text })}>
                                </TextInput>
                                <Text style={[styles.contentText]}>Jenis Pengolahan Ikan</Text>
                                <Picker
                                    id="pick"
                                    selectedValue={this.state.jenisIkan}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ jenisIkan: itemValue })}>
                                    <Picker.Item label="Pilih jenis pengolahan produk" value="" />
                                    <Picker.Item label="Ikan Segar" value="Segar" />
                                    <Picker.Item label="Ikan Olahan" value="Olahan" />
                                </Picker>
                                {(this.state.jenisIkan == "Olahan") ?
                                    <View>
                                        <Text style={[styles.contentText]}>Cara Pengolahan</Text>
                                        <Picker
                                            id="pick"
                                            selectedValue={this.state.olahan}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ olahan: itemValue })}>
                                            <Picker.Item label="Pilih cara pengolahan" value="" />
                                            <Picker.Item label="Dikeringkan" value="Dikeringkan" />
                                            <Picker.Item label="Diasinkan" value="Diasinkan" />
                                            <Picker.Item label="Diasapkan" value="Diasapkan" />
                                            <Picker.Item label="Di-fillet" value="Di-fillet" />
                                            <Picker.Item label="Dikalengkan" value="Dikalengkan" />
                                            <Picker.Item label="Dikemas Vakum" value="Divakum" />
                                        </Picker>
                                    </View> :
                                    <View></View>
                                }
                                <Text style={[styles.contentText]}>Nama Ikan</Text>
                                {/* <Picker
									selectedValue={this.state.namaIkan}
									onValueChange={(itemValue, itemIndex) => this.setState({namaIkan: itemValue})}>
									<Picker.Item label="Pilih nama Ikan" value="" />
									<Picker.Item label="Ikan Tuna" value="tuna" />
									<Picker.Item label="Ikan Tongkol" value="tongkol" />
									<Picker.Item label="Ikan Kakap" value="kakap" />
									<Picker.Item label="Ikan Lele" value="lele" />
									<Picker.Item label="Ikan Salmon" value="salmon" />
								</Picker> */}
                                <TouchableOpacity style={[styles.pickerListIkan]} onPress={this.onShowListIkan}>
                                    <Text style={[styles.pickerTextListIkan]}>{this.state.namaIkan}</Text>
                                </TouchableOpacity>
                                <ModalFilterPicker
                                    title="Pilih Nama Ikan"
                                    noResultsText="Tidak ada nama Ikan"
                                    visible={this.state.visibleListIkan}
                                    onSelect={this.onSelectListIkan}
                                    onCancel={this.onCancelListIkan}
                                    options={listIkan}
                                    titleTextStyle={[styles.titleTextStyleListIkan]}
                                    filterTextInputStyle={[styles.filterTextInputStyleListIkan]}
                                    overlayStyle={[styles.overlayStyleListIkan]}
                                    listContainerStyle={[styles.listContainerStyleListIkan]}
                                    optionTextStyle={[styles.optionTextStyleListIkan]}
                                />
                                <Text style={[styles.contentText]}>Pilih pembelian</Text>
                                <Picker
                                    selectedValue={this.state.pembelian}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ pembelian: itemValue })}>
                                    <Picker.Item label="Berat" value="Berat" />
                                    <Picker.Item label="Satuan" value="Satuan" />
                                </Picker>
                                {(this.state.pembelian == "Satuan") ?
                                    <View>
                                        <Text style={[styles.contentText]}>Jumlah dalam
										{" " + this.state.satuan}
                                        </Text>
                                        <View style={[styles.picker2]}>
                                            <TextInput
                                                id="weight"
                                                style={{ flex: 6, fontFamily: "Montserrat-Regular", color: "#666666" }}
                                                keyboardType='numeric'
                                                value={this.state.weight}
                                                onChangeText={text => this.setState({ weight: this.onInputNumeric(text) })}>
                                            </TextInput>
                                            <Picker
                                                style={{ flex: 4 }}
                                                selectedValue={this.state.satuanPicker}
                                                itemStyle={[styles.contentText]}
                                                onValueChange={(itemValue, itemIndex) => this.setModalOpen(itemValue)}>
                                                <Picker.Item label="Ekor" value="Ekor" />
                                                <Picker.Item label="Buah" value="Buah" />
                                                <Picker.Item label="Kemasan" value="Kemasan" />
                                                <Picker.Item label="Lainnya" value="Lainnya" />
                                            </Picker>
                                        </View>
                                        <Text style={[styles.contentText]}>Harga Ikan per
										{" " + this.state.satuan}
                                        </Text>
                                    </View> :
                                    <View>
                                        <Text style={[styles.contentText]}>Jumlah
										{" " + this.state.satuan}
                                        </Text>
                                        <View style={[styles.picker2]}>
                                            <TextInput
                                                id="weight"
                                                style={{ flex: 6, fontFamily: "Montserrat-Regular", color: "#666666" }}
                                                keyboardType='numeric'
                                                value={this.state.weight}
                                                onChangeText={text => this.setState({ weight: this.onInputNumeric(text) })}>
                                            </TextInput>
                                            <Picker
                                                style={{ flex: 4 }}
                                                selectedValue={this.state.satuanPicker}
                                                itemStyle={[styles.contentText]}
                                                onValueChange={(itemValue, itemIndex) => this.setModalOpen(itemValue)}>
                                                <Picker.Item label="Gram" value="Gram" />
                                                <Picker.Item label="Kilogram" value="Kilogram" />
                                                <Picker.Item label="Ons" value="Ons" />
                                                <Picker.Item label="Kuintal" value="Kuintal" />
                                                <Picker.Item label="Lainnya" value="Lainnya" />
                                            </Picker>
                                        </View>
                                        <Text style={[styles.contentText]}>Harga Ikan per
										{" " + this.state.satuan}
                                        </Text>
                                    </View>
                                }
                                <TextInput
                                    id="price"
                                    style={{ fontFamily: "Montserrat-Regular", color: "#666666" }}
                                    keyboardType='numeric'
                                    value={this.state.price}
                                    onChangeText={(text) => this.setState({ price: this.onInputNumeric(text) })}>
                                </TextInput>
                                <Text style={[styles.contentText]}>Ukuran</Text>
                                <Picker
                                    selectedValue={this.state.ukuranIkan}
                                    onValueChange={(itemValue, itemIndex) => this.setState({ ukuranIkan: itemValue })}>
                                    <Picker.Item label="Pilih ukuran Ikan" value="" />
                                    <Picker.Item label="Besar" value="Besar" />
                                    <Picker.Item label="Sedang" value="Sedang" />
                                    <Picker.Item label="Kecil" value="Kecil"
                                    />
                                </Picker>
                                <Text style={[styles.contentText]}>Deskripsi</Text>
                                <TextInput
                                    defaultValue={this.state.description}
                                    id="description"
                                    style={{ fontFamily: "Montserrat-Regular", color: "#666666" }}
                                    onChangeText={text => this.setState({ description: text })}
                                    multiline={true}>
                                </TextInput>
                                <Text style={[styles.contentText]}>Waktu Ditangkap</Text>
                                <DatePicker
                                    style={{ width: 200, marginTop: 5, marginBottom: 20 }}
                                    date={this.state.date}
                                    mode="datetime"
                                    placeholder={this.state.date}
                                    format="YYYY-MM-DD, HH:mm"
                                    is24Hour
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    androidMode="spinner"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 36
                                        },
                                        placeholderText: {
                                            fontSize: 12,
                                            color: '#666666'
                                        }
                                    }}
                                    onDateChange={(date) => { this.setState({ date: date }) }}
                                />
                            </View>
                        </ScrollView>
                    </View>
                    <View style={styles.footer}>
                        <TouchableHighlight id="add" style={styles.buttonFooter} onPress={() => { this.showAlertConfirm() }}>
                            <Text style={[styles.textFooter]}>Submit</Text>
                        </TouchableHighlight>
                    </View>
                    <AlertUI
                        show={this.state.showAlertConfirm}
                        showProgress={false}
                        title="Konfimasi"
                        message="Apa anda yakin ingin mengedit lapak?"
                        closeOnTouchOutside={false}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={true}
                        cancelText="Batal"
                        confirmText="Edit"
                        confirmButtonColor="#1E90FF"
                        cancelButtonColor="#999999"
                        titleStyle={{ fontSize: 16 }}
                        messageStyle={{ fontSize: 16 }}
                        cancelButtonTextStyle={{ fontSize: 16 }}
                        confirmButtonTextStyle={{ fontSize: 16 }}
                        onCancelPressed={() => {
                            this.hideAlertConfirm();
                        }}
                        onConfirmPressed={() => {
                            this.hideAlertConfirm();
                            this.addToFirebase();
                        }}
                    />
                    <AlertUI
                        show={this.state.showAlertError}
                        showProgress={false}
                        title="Error"
                        message={this.state.valid}
                        closeOnTouchOutside={false}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={false}
                        cancelText="Tutup"
                        cancelButtonColor="#999999"
                        titleStyle={{ fontSize: 16 }}
                        messageStyle={{ fontSize: 16 }}
                        cancelButtonTextStyle={{ fontSize: 16 }}
                        onCancelPressed={() => {
                            this.hideAlertError();
                        }}
                    />
                    <AlertUI
                        show={this.state.showAlertBack}
                        title="Apa anda yakin untuk kembali?"
                        closeOnTouchOutside={false}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={true}
                        cancelText="Tidak"
                        confirmText="Ya"
                        cancelButtonColor="#999999"
                        confirmButtonColor="#1E90FF"
                        titleStyle={{ fontSize: 16 }}
                        messageStyle={{ fontSize: 16 }}
                        cancelButtonTextStyle={{ fontSize: 16 }}
                        confirmButtonTextStyle={{ fontSize: 16 }}
                        onCancelPressed={() => {
                            this.hideAlertBack();
                        }}
                        onConfirmPressed={() => {
                            this.props.navigation.dispatch(NavigationActions.back())
                        }}
                    />
                    <Modal style={[styles.modal]}
                        position={"center"}
                        isOpen={this.state.modalVisible}
                        backdropPressToClose={false}>
                        <View style={[styles.contentModal]}>
                            <Text style={styles.contentTextModal}>Masukkan Pilihan Lain Anda</Text>
                            <TextInput
                                id="modal"
                                style={{ fontFamily: "Montserrat-Regular", color: "#666666" }}
                                value={this.state.modalText}
                                onChangeText={(text) => this.setState({ modalText: text })} />
                        </View>
                        <Button onPress={() => this.setState({ modalVisible: !this.state.modalVisible, satuan: this.state.modalText })} style={styles.btn} title="Ok" />
                    </Modal>
                    <Spinner visible={this.state.modalLoad} color='#1E90FF' overlayColor={'rgba(240, 248, 255, 0.7)'} />
            </View>
        );
    }

    onBackButtonPressAndroid = () => {
        if (!this.state.showAlertConfirm && !this.state.showAlertBack && !this.state.showAlertError) {
            this.showAlertBack()
            return true;
        }
        return false;
    };

    onShowListIkan = () => {
        this.setState({ visibleListIkan: true });
    }

    onSelectListIkan = (picked) => {
        this.setState({
            namaIkan: picked,
            visibleListIkan: false
        })
    }

    onCancelListIkan = () => {
        this.setState({
            visibleListIkan: false
        });
    }

    setModalOpen(itemVal) {
        if (itemVal == "Lainnya") {
            this.setState({ modalVisible: true });
            this.setState({ satuanPicker: itemVal });
            this.setState({ modalText: '' })
        } else {
            this.setState({ satuanPicker: itemVal });
            this.setState({ satuan: itemVal });
        }
    }
    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    showAlertConfirm = () => {
        this.setState({
            showAlertConfirm: true
        });
    };

    hideAlertConfirm = () => {
        this.setState({
            showAlertConfirm: false
        });
    };

    showAlertError = () => {
        this.setState({
            showAlertError: true
        });
    };

    showAlertBack = () => {
        this.setState({
            showAlertBack: true
        });
    };

    hideAlertBack = () => {
        this.setState({
            showAlertBack: false
        });
    };

    hideAlertError = () => {
        this.setState({
            showAlertError: false
        });
    };

    onInputNumeric(text) {
        text = text.replace(/[^0-9]/g, '')
        return text.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    }

    changeToNumeric(text) {
        text = text.replace(/[^0-9]/g, '')
        return Number(text)
    }

    constructor(props) {
        super(props);
        this.berandaRef = firebase.database().ref().child('beranda');
        this.detailRef = firebase.database().ref().child('productDetails');
        this.userRef = firebase.database().ref('users/' + uniqueId);
        const { params } = this.props.navigation.state;
        const detail = params ? params.detail : '';
        const basic = params ? params.basic : '';
        dateNow = new Date(basic.timestamp * 1000);
        date = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + "-" + dateNow.getDate() + ", " + dateNow.getHours() + ":" + dateNow.getMinutes()
        const tipe = (basic.satuanIkan == 'Ekor' || basic.satuanIkan == 'Buah' || basic.satuanIkan == 'Kemasan' || basic.satuanIkan == 'Lainnya') ? 'Satuan' : 'Berat';
        this.state = {
            imageSource1: null,
            namaIkan: basic.namaIkan,
            location: basic.location,
            weight: detail.weight + '',
            price: basic.price + '',
            title: basic.judul,
            description: detail.description,
            pembelian: tipe,
            isImage1: false,
            jenisIkan: detail.jenisProduk,
            kondisiIkan: detail.kondisiIkan,
            ukuranIkan: detail.ukuran + '',
            date: date,
            olahan: detail.caraOlah,
            satuan: basic.satuanIkan,
            satuanPicker: basic.satuanIkan,
            showAlertConfirm: false,
            showAlertError: false,
            showAlertBack: false,
            modalVisible: false,
            modalText: '',
            modalLoad: false,
            visibleListIkan: false,
        };
    }

    addTodo() {
        this.showAlertConfirm();
    }

    addToFirebase() {
        let valid = this.validateForm(this.state);
        this.setState({ valid: valid });

        const { params } = this.props.navigation.state;
        const basic = params ? params.basic : '';

        if (valid != '') {
            this.showAlertError()
        } else {
            let time = Number(this.convertDateToTimestamp(this.state.date));
            if (this.state.jenisIkan == "Olahan") {
                this.state.kondisiIkan = this.state.jenisIkan + " dengan cara " + this.state.olahan
            } else {
                this.state.kondisiIkan = this.state.jenisIkan
            }

            var price = this.changeToNumeric(this.state.price);
            var judul = this.state.title;
            var namaIkan = this.state.namaIkan;
            var satuanIkan = this.state.satuan;

            var query1 = firebase.database().ref('beranda').orderByChild('productId').equalTo(basic.productId);
            var post1 = query1.once("child_added", function (snapshot) {
                snapshot.ref.update({
                    price: price,
                    judul: judul,
                    timestamp: time,
                    namaIkan: namaIkan,
                    satuanIkan: satuanIkan,
                })
            });

            var query2 = firebase.database().ref('productDetails/' + basic.productId);
            var post2 = query2.ref.update({
                description: this.state.description,
                weight: this.changeToNumeric(this.state.weight),
                jenisProduk: this.state.jenisIkan,
                ukuran: this.state.ukuranIkan,
                kondisiIkan: this.state.kondisiIkan,
                caraOlah: this.state.olahan,
            })

            this.props.navigation.pop(2)
        }
    }

    convertDateToTimestamp(timestamp) {
        let arrayDate = timestamp.split("-")
        let arrayHours = arrayDate[2].split(":")
        var date = new Date(arrayDate[0], arrayDate[1] - 1, arrayDate[2].substring(0, 2), arrayHours[0].slice(-2), arrayHours[1], 0, 0)
        timestampDate = date.getTime() + ""
        let time = "" + timestampDate.substring(0, 10)
        return time
    }

    validateForm(state) {
        let strValid = "";
        if (state.title == '') {
            strValid = "Isi judul dari ikan \n";
        } else if (state.jenisIkan == '') {
            strValid = "Isi jenis pengolahan produk \n";
        } else if (state.jenisIkan == 'Olahan' && state.olahan == '') {
            strValid = "Isi cara pengolahan ikan \n";
        } else if (state.namaIkan == 'Pilih Nama Ikan') {
            strValid = "Isi nama dari ikan \n";
        } else if (state.weight == '') {
            strValid = "Isi jumlah/bobot dari ikan \n";
        } else if (state.price == '') {
            strValid = "Isi harga dari ikan \n";
        } else if (state.ukuranIkan == '') {
            strValid = "Isi ukuran dari ikan \n";
        } else if (state.description == '') {
            strValid = "Isi deskripsi dari ikan \n";
        } else if (state.date == '') {
            strValid = "Isi tanggal ikan ditangkap \n";
        }
        return strValid
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: "#f0f8ff"
    },
    content: {
        marginTop: 5,
        marginBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    contentText: {
        fontSize: 18,
        fontFamily: "Montserrat-Regular",
        color: "#666666"
    },
    contentModal: {
        marginTop: 5,
        marginBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    contentTextModal: {
        fontSize: 15,
        fontFamily: "Montserrat-Regular",
        color: "#666666"
    },
    textHeader: {
        fontSize: 36,
        color: "white",
        fontWeight: "bold",
        textAlign: "left",
        alignSelf: "stretch"
    },
    textFooter: {
        fontSize: 20,
        color: "white",
        fontFamily: "Helvetica-Bold"

    },
    imageContainer: {
        flex: 1
    },
    image: {
        width: 100,
        height: 100,
    },
    footer: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#1e90ff'
    },
    buttonFooter: {
        alignItems: 'center',
        padding: 10
    },
    picker2: {
        flex: 1,
        flexDirection: 'row'
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 300,
        width: 300
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },
    containerLoading: {
        justifyContent: 'center'
    },
    pickerListIkan: {
        marginTop: 15,
        marginBottom: 15
    },
    pickerTextListIkan: {
        fontSize: 15,
        fontFamily: 'sans-serif-light',
        color: '#666666'
    },
    overlayStyleListIkan: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: '#333333',
        justifyContent: 'center',
        alignItems: 'center'
    },
    listContainerStyleListIkan: {
        flex: 1,
        width: width * 0.8,
        maxHeight: height * 0.7,
        backgroundColor: '#fff',
        borderRadius: 10,
        marginBottom: 15,
        overflow: 'hidden'
    },
    optionTextStyleListIkan: {
        fontSize: 15,
        fontFamily: "Montserrat-Regular"
    },
    filterTextInputStyleListIkan: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        flex: 0,
        height: 50,
        fontFamily: "Montserrat-Regular",
        fontSize: 15
    },
    titleTextStyleListIkan: {
        fontFamily: "Helvetica-Bold",
        color: "white",
        fontSize: 20,
        flex: 0,
        marginBottom: 15
    },
    noResultsText: {
        flex: 1,
        textAlign: 'center',
        color: '#ccc',
        fontStyle: 'italic',
        fontSize: 22
    }
});