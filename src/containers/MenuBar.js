import { AppRegistry} from "react-native";
import { TabNavigator , TabBarTop, StackNavigator} from "react-navigation"; // Version can be specified in package.json
import ProductsFeed from "./Products";
import Pesan from "./DaftarPesan";
import Profile from "./ProfilPengguna";
import Profil from "./Profil";
import Edit from "./EditProdukJual";
import DetailsScreen from "./ProductDetails";
import DetailsScreenBeli from "./ProductDetailsBeli.js"
import RuangPesan from "./Pesan";
import Search from "./SearchQuery";
import React from "react";
import PropTypes from "prop-types";
import BuatLapakJual from "./BuatLapakJual";
import MenuLapak from "./MenuLapak";
import MenuBuatLapak from "./MenuBuatLapak";
import { Icon } from 'react-native-elements';

const HomeTabBarIcon = ({ tintColor }) => (
	<Icon name='home' size={25} color={tintColor} />
);

HomeTabBarIcon.propTypes = {
	tintColor: PropTypes.string.isRequired,
};

const LapakTabBarIcon = ({ tintColor }) => (
	<Icon name='shopping-basket' size={25} color={tintColor} />
);

LapakTabBarIcon.propTypes = {
	tintColor: PropTypes.string.isRequired,
};

const PesanTabBarIcon = ({ tintColor }) => (
	<Icon name='chat' size={25} color={tintColor} />
);

PesanTabBarIcon.propTypes = {
	tintColor: PropTypes.string.isRequired,
};

const ProfileTabBarIcon = ({ tintColor }) => (
	<Icon name='person' size={25} color={tintColor} />
);

ProfileTabBarIcon.propTypes = {
	tintColor: PropTypes.string.isRequired, 
};

export default TabNavigator (
	{
		Beranda: { 
			screen: StackNavigator(
				{
					Products: {
						screen: ProductsFeed,
						navigationOptions : {
							header : false
						}
					},
					DetailsJual: {
						screen: DetailsScreen,
						navigationOptions : {
							header: false,
						}
					},
					DetailsBeli: {
						screen: DetailsScreenBeli,
						navigationOptions : {
							header: false,
						}
					},
					RuangPesan: {
						screen: RuangPesan,
					},
					Profil: {
						screen: Profil,
						navigationOptions: {
							tabBarVisible: false,
							header: false
						}
					},
					Edit: {
						screen: Edit,
						navigationOptions: {
							tabBarVisible: false,
							title: "Edit Lapak",
							headerStyle: {
								backgroundColor: '#1e90ff',
							},
							headerTintColor: '#fff',
							headerTitleStyle: {
								fontWeight: "normal",
								fontSize: 20,
								fontFamily: 'Helvetica-Bold'
							}
						}
					},
					Search: {
						screen: Search,
						navigationOptions: {
							header: false,
						}
					}
				},
				{
					initialRouteName: "Products",
				}
			),
			navigationOptions: {
				tabBarIcon: HomeTabBarIcon
			},
		},
		Lapak: { 
			screen: StackNavigator({
				MenuLapak: { 
					screen: MenuLapak,
					navigationOptions: {
						title: "Lapak"
					}
				},
				MenuBuatLapak: { 
					screen: MenuBuatLapak,
					navigationOptions: {
						tabBarVisible: false,
						title: "Buat Lapak",
						headerStyle: {
							backgroundColor: '#1e90ff',
						},
						headerTintColor: '#fff',
						headerTitleStyle: {
							fontWeight: "normal",
							fontSize: 20,
							fontFamily: 'Helvetica-Bold'
						}
					}
				},
				DetailsJual: {
					screen: DetailsScreen,
					navigationOptions : {
						header: false,
					}
				},
				DetailsBeli: {
					screen: DetailsScreenBeli,
					navigationOptions : {
						header: false,
					}
				},
				Edit: {
					screen: Edit,
					navigationOptions: {
						tabBarVisible: false,
						title: "Edit Lapak",
						headerStyle: {
							backgroundColor: '#1e90ff',
						},
						headerTintColor: '#fff',
						headerTitleStyle: {
							fontWeight: "normal",
							fontSize: 20,
							fontFamily: 'Helvetica-Bold'
						}
					}
				},
				Profil: {
					screen: Profil,
					navigationOptions: {
						tabBarVisible: false,
						header: false
					}
				},
				RuangPesan: {
					screen: RuangPesan,
				},
			},
			{
				initialRouteName: "MenuLapak",
			}),
			navigationOptions: {
				tabBarIcon: LapakTabBarIcon,
				label: 'Lapak'
			},

		},
		Pesan: { 
			screen: StackNavigator(
				{
					DaftarPesan: {
						screen: Pesan,
					},
					RuangPesan: {
						screen: RuangPesan,
					},
				},
				{
					initialRouteName: "DaftarPesan",
				}
			),
			navigationOptions: {
				tabBarIcon: PesanTabBarIcon
			},
		},
		Profile: { 
			screen: StackNavigator({
				Profile: {
					screen: Profile,
					navigationOptions: {
						header: false,
					}
				},
				DetailsJual: { 
					screen: DetailsScreen,
					navigationOptions: {
						header: false,
					}
				},
				DetailsBeli: { 
					screen: DetailsScreenBeli,
					navigationOptions: {
						header: false,
					}
				},
				Profil: {
					screen: Profil,
					navigationOptions: {
						tabBarVisible: false,
						header: false
					}
				},
				Edit: {
					screen: Edit,
					navigationOptions: {
						tabBarVisible: false,
						title: "Edit Lapak",
						headerStyle: {
							backgroundColor: '#1e90ff',
						},
						headerTintColor: '#fff',
						headerTitleStyle: {
							fontWeight: "normal",
							fontSize: 20,
							fontFamily: 'Helvetica-Bold'
						}
					}
				},
				RuangPesan: {
					screen: RuangPesan,
				},
			},
			{
				initialRouteName: "Profile"
			},
		),
			navigationOptions: {
				tabBarIcon: ProfileTabBarIcon
			}, 
		},
	},
	{	
		tabBarComponent: TabBarTop,
		tabBarPosition: "bottom",
		tabBarOptions: {
			activeTintColor: "white",
			showLabel : true,
			labelStyle: {
				fontSize: 14,
				margin:2,
				fontFamily: "Montserra-Regular"
			},
			iconStyle : {
				margin:2
			},
			allowFontScaling : true,
			showIcon :true,
			indicatorStyle: { 
				backgroundColor: "lightskyblue",
				height: 5},
			style: {backgroundColor: "dodgerblue", height: 70, padding:0, margin:0},
		},
		animationEnabled: false,
		swipeEnabled: false,
	}
);

AppRegistry.registerComponent("PPLA8kedua", () => TabNavigator);
