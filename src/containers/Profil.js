import React, { Component } from 'react';
import { AppRegistry, View, Text, Image, ScrollView, TouchableHighlight, StyleSheet, Alert, ListView, FlatList } from 'react-native';
// import * as firebase from 'firebase';
import { StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';
import ProductDetails from "./ProductDetails";
import ProductsItem from "../components/ProductsItem";
import firebase from "../firebase.js";
var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class Profil extends Component {
    constructor(props) {
        super(props);
        this.productsRef = firebase.database().ref().child("beranda/");
        this.state = {
            productsSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 != row2 }),
            username: '',
            sameId: false,
            id: '',
        };
        this.products = [];
    }

    componentDidMount() {
        const { params } = this.props.navigation.state;
        const id = params.id;
        this.setState({
            id: params.id,
            username: params.username,
        });

        this.productsRef.orderByChild('userId').equalTo(id).on("child_added", (dataSnapshot) => {
            this.products.push({ id: dataSnapshot.key, product: dataSnapshot.val() });
            this.setState({
                productsSource: this.state.productsSource.cloneWithRows(this.products)
            });
            this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
        });

        this.productsRef.orderByChild('userId').equalTo(id).on("child_changed", (dataSnapshot) => {
            this.products = this.products.filter((x) => x.id != dataSnapshot.key);
            this.products.push({ id: dataSnapshot.key, product: dataSnapshot.val() });
            this.setState({
                productsSource: this.state.productsSource.cloneWithRows(this.products)
            });
            this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
        });

        this.productsRef.orderByChild('userId').equalTo(id).on("child_removed", (dataSnapshot) => {
            this.products = this.products.filter((x) => x.id != dataSnapshot.key);
            this.setState({
                productsSource: this.state.productsSource.cloneWithRows(this.products)
            });
            this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
        });
    }

    componentWillUnmount() {
        this.productsRef.off();
    }

    renderButton() {
        if(this.state.sameId == true){
    
        } else {
          if(this.state.id == uniqueId){
            this.setState({sameId: true});
          }
        }
    
        if (this.state.sameId) {
            return null;
        } else {
            return (
                <View style={{ flex: 0.1, flexDirection: 'row', padding: 10 }}>
                        <View style={{ flex: 0.5, padding: 5 }}>
                                <TouchableHighlight
                                    underlayColor="#F0F8FF"
                                    onPress={() => {
                                        this.props.navigation.navigate("RuangPesan", {
                                            friendId: this.state.id,
                                        });
                                    }}
                                >
                                <View style={{ borderColor: '#D3D3D3', borderWidth: 1, borderRadius: 5, alignItems: 'center', padding: 5 }}>
                                    <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                        Chat Penjual
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                        <View style={{ flex: 0.5, padding: 5 }}>
                            <TouchableHighlight underlayColor="#F0F8FF" onPress={() => Alert.alert("Fitur ini masih sedang dalam tahap pengembangan")}>
                                <View style={{ backgroundColor: '#1E90FF', borderRadius: 5, alignItems: 'center', padding: 5 }}>
                                    <Text style={{ color: '#F0F8FF', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                        Jadi Langganan
                                    </Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
            );
        }
    }

    nav(tipeLapak, item) {
		if(tipeLapak == "Jual") {
			this.props.navigation.navigate('DetailsJual', {
				product: item.product,
			});
		} else {
			this.props.navigation.navigate('DetailsBeli', {
				product: item.product,
			});
		}
	}

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#F0F8FF' }}>
                <View style={{ flex: 0.2, backgroundColor: '#1E90FF', flexDirection: 'row' }}>
                    <View style={{ flex: 0.3, margin: 5, alignSelf: 'center' }}>
                        <Icon
                            name='shop'
                            type='entypo'
                            color='white'
                            size={72}
                        />
                    </View>
                    <View style={{ flex: 0.7, padding: 10, flexDirection: 'column' }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: 'white', fontFamily: 'montserrat-regular', fontSize: 36, fontWeight: 'bold' }}>
                                {this.state.username}
                            </Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={{ flex: 0.15, alignSelf: 'center' }}>
                                <Icon
                                    name='location-pin'
                                    type='entypo'
                                    color='#D3D3D3'
                                    size={24}
                                />
                            </View>
                            <View style={{ flex: 0.8, alignSelf: 'center' }}>
                                <Text style={{ color: '#D3D3D3', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                    Jakarta
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 0.8, backgroundColor: '#F0F8FF' }}>
                    {this.renderButton()}
                    {/* <View style={{ flex: 0.2, flexDirection: 'row', padding: 10 }}>
                        <View style={{ flex: 0.5, borderRightColor: '#333333', borderRightWidth: 1, flexDirection: 'column' }}>
                            <View style={{ flex: 0.2 }}>
                                <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20, alignSelf: 'center' }}>
                                    Rekomendasi
                                </Text>
                            </View>
                            <View style={{ flex: 0.8, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ color: '#999999', fontFamily: 'montserrat-regular', fontSize: 60 }}>
                                    100
                                </Text>
                            </View>
                        </View>
                        <View style={{ flex: 0.5, flexDirection: 'column' }}>
                            <View style={{ flex: 0.2 }}>
                                <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20, alignSelf: 'center' }}>
                                    Kecepatan Respon
                                </Text>
                            </View>
                            <View style={{ flex: 0.8, alignItems: 'center', justifyContent: 'center' }}>
                                <Icon
                                    name='clock-fast'
                                    type='material-community'
                                    color='#333333'
                                    size={32}
                                />
                                <Text style={{ color: '#999999', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                    Cepat
                                </Text>
                            </View>
                        </View>
                    </View> */}
                    <View style={{ flex: 0.1, backgroundColor: 'white' }}>
                        <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20, padding: 10 }}>
                            Produk
                        </Text>
                    </View>
                    <View style={{ flex: 0.8 }}>
                        <FlatList
                            data={this.products}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({ item, index }) => (
                                <TouchableHighlight
                                    underlayColor="#D3D3D3"
                                    onPress={() => this.nav(item.product.tipeLapak, item)}
                                >
                                    <ProductsItem index={index} products={item.product} />
                                </TouchableHighlight>
                            )}
                        />
                    </View>
                </View>
                
            </View>
        );
    }
};