import React, { Component } from "react";
import { StyleSheet,Text,View,ListView,FlatList,TouchableHighlight,Alert,Modal,TextInput,Button} from "react-native";
import { StackNavigator } from "react-navigation";
import ProductsItem from "../components/ProductsItem";
import DetailsScreen from "./ProductDetails";
import Pesan from "./Pesan";
import Search from "./SearchQuery";
import { SearchBar, Icon } from 'react-native-elements';
import firebase from "../firebase.js";
import { searchBarStylesheet } from "../assets/stylesheet/productDetailsStyle";

var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class Products extends Component {
	static navigationOptions = {
		header:(
				<SearchBar
					lightTheme
					placeholder = 'Search'
					placeholderTextColor='#999999'
					inputStyle={{color: '#333333', backgroundColor: '#ffffff', fontFamily: 'Helvetica-Regular', fontSize:14}}
					returnKeyType='search'
					containerStyle={{backgroundColor: '#1e90ff'}}
					round
					onSubmitEditing={() => null}
					clearIcon
				/>
		)
	};

	constructor(props) {
		super(props);

		this.productsRef = firebase.database().ref().child("beranda");
		this.state = {
			productsSource: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 != row2}),
			modalVisible: false,
			newName: "",
		};
		this.products = [];

		firebase.database().ref().child("users/" + uniqueId + "/nama").on("value", (snapshot) => {
			if(snapshot.val() == null){
				this.setModalVisible(true);
			}
		});
	}

	setModalVisible(visible) {
		this.setState({modalVisible: visible});
	}

	setName(nama) {
		if(nama != ""){
			firebase.database().ref("users/" + uniqueId).update({
				nama: nama
			});
			this.setModalVisible(!this.state.modalVisible);
		} else {
			Alert.alert("Nama tidak boleh kosong");
		}
	}

	componentDidMount() {
		this.productsRef.on("child_added", (dataSnapshot) => {
			this.products.push({id: dataSnapshot.key, product: dataSnapshot.val()});
			this.setState({
				productsSource: this.state.productsSource.cloneWithRows(this.products)
			});
			console.log(this.products[0].product.timestamp)
			this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
		});

		this.productsRef.on("child_changed", (dataSnapshot) => {
			this.products = this.products.filter((x) => x.id != dataSnapshot.key);
			this.products.push({id: dataSnapshot.key, product: dataSnapshot.val()});
			this.setState({
				productsSource: this.state.productsSource.cloneWithRows(this.products)
			});
			this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
		});

		this.productsRef.on("child_removed", (dataSnapshot) => {
			this.products = this.products.filter((x) => x.id != dataSnapshot.key);
			this.setState({
				productsSource: this.state.productsSource.cloneWithRows(this.products)
			});
			this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
		});
		// SplashScreen.hide();
	}

	componentWillUnmount() {
		this.productsRef.off();
	}

	compare(a,b) {
		if (a.timestamp < b.timestamp)
			return -1;
		if (a.timestamp > b.timestamp)
			return 1;
		return 0;
	}

	nav(tipeLapak, item) {
		if(tipeLapak == "Jual") {
			this.props.navigation.navigate('DetailsJual', {
				product: item.product,
			});
		} else {
			this.props.navigation.navigate('DetailsBeli', {
				product: item.product,
			});
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={this.state.modalVisible}>

					<View style={styles.modal_container}>
						<Text>Nama Saya adalah</Text>
						<View style={styles.text_input_view}>
							<TextInput ref={input => { this.textInput = input; }}
								style={styles.text_input}
								placeholder="Nama"
								onChangeText={(newName) => this.setState({newName})}
							/>
						</View>
						<View style={styles.button_view}>
							<Button
								title="OK"
								onPress={() => 
									this.setName(this.state.newName)					
								}>
							</Button>
						</View>
					</View>
				</Modal>
				<View style={styles.container}>
					<View style={searchBarStylesheet.background}>
						<TouchableHighlight style={searchBarStylesheet.subContainer}
						onPress={() => {
						this.props.navigation.navigate('Search');
						}}
						>
						<View style={searchBarStylesheet.mainContainer}>
						<View style={searchBarStylesheet.container}>
							<Icon
							name='md-search'
							type='ionicon'
							color='#999999'
							size={15}
							/>
						</View>
						<Text style={searchBarStylesheet.content}>
							Search
									</Text>
						</View>
						</TouchableHighlight>
					</View>
					<FlatList
						data={this.products}
						keyExtractor={(item, index) => item.id}
						renderItem={({item, index}) => (
							<TouchableHighlight 
								underlayColor="#D3D3D3"
								onPress={() => this.nav(item.product.tipeLapak, item)}
								style={styles.button}>
								<ProductsItem index={index} products={item.product} />
							</TouchableHighlight>
						)}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#F0F8FF"
	},
	modal_container: {
		flex: 1,
		backgroundColor: "#F0F8FF",
		alignItems: "center",
		justifyContent: "center"
	},
	button: {
		alignItems: "center",
		justifyContent: "center",
		paddingRight: 10,
		paddingLeft: 10
	},
	text_input: {
		flex: 1,
		textAlign: "center"
	},
	text_input_view: {
		height: 40,
		width: 250,
		flexDirection: "row",
		marginTop: 20
	},
	button_view: {
		height: 40,
		width: 250,
		marginTop: 20
	}
});
