import React from "react";
import { Text, View, StyleSheet, ListView, TextInput, Button, Alert, ScrollView, Dimensions } from "react-native";
import firebase from "../firebase.js";
import { List, ListItem } from "react-native-elements";
import { StackNavigator } from "react-navigation";
import Timestamp from "react-timestamp";

var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class Pesan extends React.Component {
	static navigationOptions = {
		title: "Ruang Pesan",
		tabBarVisible: false,
		headerStyle: {
			backgroundColor: '#1e90ff',
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontWeight: "normal",
			fontSize: 20,
			fontFamily: 'Helvetica-Bold'
		},
	};

	constructor(props) {
		super(props);

		this.state = {
			listViewData: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 != row2}),
			newChat: ""
		};
		this.chats = [];
		const { params } = this.props.navigation.state;
		this.friendId = params ? params.friendId : '';
		this.username = "";
        this.friendName = "";
        var {height, width} = Dimensions.get('window');
        this.maxWidth = 0.8 * width;
	}

	componentDidMount() {
		firebase.database().ref().child("chats/" + uniqueId + "/" + this.friendId).on("child_added", (dataSnapshot) => {
			if(/^\d{10}$/.test(dataSnapshot.key)){
				this.chats.push({
					id: dataSnapshot.child("id").val(), 
					chat: dataSnapshot.child("chat").val(), 
					nama: dataSnapshot.child("name").val(),
					time: dataSnapshot.child("time").val()
				});
				this.setState({
					listViewData: this.state.listViewData.cloneWithRows(this.chats)
				});
			}
		});

		firebase.database().ref().child("chats/" + uniqueId + "/" + this.friendId).on("child_removed", (dataSnapshot) => {
			this.chats = this.chats.filter((x) => x.id != dataSnapshot.key);
			this.setState({
				listViewData: this.state.listViewData.cloneWithRows(this.chats)
			});
		});

		firebase.database().ref("users/" + uniqueId + "/nama").on("value", (snapshot) => {
			this.username = snapshot.val();
		});

		firebase.database().ref("users/" + this.friendId + "/nama").on("value", (snapshot) => {
			this.friendName = snapshot.val();
        });
        
        // firebase.database().ref("chats/" + uniqueId + "/" + this.friendId).update({
		// 	read : true
		// });
	}

	addRow(chat) {

		var now = Date.now();
		now = now.toString().substring(0,10);
		
		firebase.database().ref("chats/" + this.friendId + "/" + uniqueId).update({
			name : this.username,
            lastChat : chat,
            lastChatTime : now
		});

		firebase.database().ref("chats/" + uniqueId + "/" + this.friendId).update({
			name : this.friendName,
			lastChat : chat,
            lastChatTime : now
		});

		firebase.database().ref("chats/" + this.friendId + "/" + uniqueId).child(now).set({
			id : uniqueId,
			chat : chat,
			name : this.username,
			time : now
		});

		firebase.database().ref("chats/" + uniqueId + "/" + this.friendId).child(now).set({
			id : uniqueId,
			chat : chat,
			name : this.username,
			time : now
		});

		this.textInput.clear()
		this.setState({newChat: ""})
	}
	  
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.container}>
					<ScrollView
					ref={ref => this.scrollView = ref}
					onContentSizeChange={(contentWidth, contentHeight)=>{        
						this.scrollView.scrollToEnd({animated: true});
					}}>
						<List>
							{
								this.chats.map((item, i) => (
									<ListItem
										key={i}
										title={
											<View>
												<Text style={item.id === uniqueId ? styles.align_right_id : styles.align_left_id }>{item.nama}</Text>
											</View>			
										}
										subtitle={
											<View style={item.id === uniqueId ? styles.align_right : styles.chat_item}>
												<View style={item.id === uniqueId ? styles.hide : [styles.bubble,{maxWidth: this.maxWidth}] }>
													<Text>{item.chat}</Text>
												</View>
                                                <View style={styles.chat_time}>
                                                    <Text><Timestamp component={Text} format='time' time={item.time} /> </Text>
                                                </View>
                                                <View style={item.id === uniqueId ? [styles.bubble,{maxWidth: this.maxWidth}] : styles.hide }>
                                                    <Text>{item.chat}</Text>
                                                </View>
											</View>
										}
										rightIcon={<View></View>}
										containerStyle={{marginTop: -1,borderTopWidth : 0, borderBottomWidth : 0, backgroundColor: "#F0F8FF"} }
									/>
								))
							}
						</List>
					</ScrollView>
				</View>

				<View style={styles.text_input_view}>
					<TextInput ref={input => { this.textInput = input }}
						style={styles.container}
						placeholder="Ketik Pesan"
						onChangeText={(newChat) => this.setState({newChat})}
					/>
					<Button
						style={styles.container}
						title="Kirim"
						onPress={() => 
							this.state.newChat != "" ? this.addRow(this.state.newChat) : {}							
						}>
					</Button>
				</View>	
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#F0F8FF"
	},
	text_input_view: {
		height: 40,
		flexDirection: 'row'
	},
	align_right: {
        flex: 1,
		flexDirection: "row",
		alignSelf: "flex-end"
	},
	align_right_id: {
		fontSize: 16,
		alignSelf: "flex-end",
		fontWeight: "bold"
    },
    align_left_id: {
		fontSize: 16,
		fontWeight: "bold"
    },
	hide: {
		height: 0,
		width: 0
    },
    chat_item: {
        flex: 1,
		flexDirection: "row"
    },
    chat_time: {
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:7,
        paddingRight:7
    },
    bubble: {
        paddingTop:5,
        paddingBottom:5,
        paddingLeft:7,
        paddingRight:7,
        backgroundColor:'#fff',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#000',
        flex:0
      },
});