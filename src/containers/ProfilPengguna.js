import React, { Component } from 'react';
import { AppRegistry, View, Text, Image, ScrollView, TouchableHighlight, StyleSheet, Alert } from 'react-native';
// import * as firebase from 'firebase';
import { StackNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';
import Profil from './Profil';
import firebase from "../firebase.js";
var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class ProfilPengguna extends Component {
    constructor(props) {
        super(props);
        // this.prodId = '';
        // this.userId = '';
        this.state = {
        //   detail: {
        //     jenisProduk: '',
        //     ukuran: '',
        //     weight: '',
        //     kondisiIkan: '',
        //     description: '',
        //   },
            username: '',
        }
    }

    componentDidMount() {
        this.userRef = firebase.database().ref('users/' + uniqueId);
        this.userRef.on('value', (snaps) => {
            this.setState({
                username: snaps.val().nama,
            })
        });
    }

    //   componentDidMount() {
    //     const { params } = this.props.navigation.state;
    //     const product = params ? params.product : '';
    //     this.productRef = firebase.database().ref('productDetails/' + product.productId);
    //     this.productRef.on('value', (snap) => {
    //       this.setState({
    //         detail: snap.val(),
    //       });
    //       this.userId = snap.val().userId;
    //       this.userRef = firebase.database().ref('users/' + this.userId);
    //       this.userRef.on('value', (snaps) => {
    //         this.setState({
    //           username: snaps.val().nama,
    //         })
    //       });
    //     });
    //   }

    render() {
        return (
            // <ScrollView style={{ flex: 1, backgroundColor: '#F0F8FF' }}>
            //     <HeaderDetails
            //         imgSource={product.imageURL}
            //         title={product.judul}
            //         price={product.price}
            //         location={product.location}
            //         timestamp={product.timestamp}
            //     />
            //     <ProductInfo
            //         jenisProduk={this.state.detail.jenisProduk}
            //         ukuran={this.state.detail.ukuran}
            //         weight={this.state.detail.weight}
            //         kondisi={this.state.detail.kondisiIkan}
            //     />
            //     <ProductDesc
            //         description={this.state.detail.description}
            //     />
            //     <SellerInfo seller={this.state.username} />
            // </ScrollView>
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={{ flex: 0.2, backgroundColor: '#1E90FF', flexDirection: 'row' }}>
                    <View style={{ flex: 0.3, margin: 10, alignSelf: 'center' }}>
                        <Icon
                            name='user-circle'
                            type='font-awesome'
                            color='white'
                            size={60}
                        />
                    </View>
                    <View style={{ flex: 0.7, alignSelf: 'center' }}>
                        <Text style={{ color: 'white', fontFamily: 'montserrat-regular', fontSize: 28, fontWeight: 'bold' }}>
                            {this.state.username}
                        </Text>
                        {/* <Text style={{ color: 'white', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                            Member sejak dulu
                        </Text> */}
                        <TouchableHighlight onPress={() => {
                            underlayColor="#1e90ff"
                            this.props.navigation.navigate('Profil', {
                                id: uniqueId,
                                username: this.state.username,
                            });
                        }}>
                            <Text style={{ color: '#D3D3D3', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                SEE FULL PROFILE
                            </Text>
                        </TouchableHighlight>
                    </View>
                </View>
                <View style={{ flex: 0.8, backgroundColor: '#F0F8FF' }}>
                    {/* <View style={{ flex: 0.3, flexDirection: 'row', padding: 10 }}>
                        <View style={{ flex: 0.5, borderRightColor: '#333333', borderRightWidth: 1, flexDirection: 'column' }}>
                            <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20, alignSelf: 'center' }}>
                                Rekomendasi
                            </Text>
                        </View>
                        <View style={{ flex: 0.5, flexDirection: 'column' }}>
                            <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20, alignSelf: 'center' }}>
                                Kecepatan Respon
                            </Text>
                        </View>
                    </View> */}
                    <View style={{ flex: 0.15 }}>
                        <TouchableHighlight style={{ flex: 1 }} underlayColor="#F0F8FF" onPress={() => Alert.alert("Fitur ini masih sedang dalam tahap pengembangan")}>
                        
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.1, padding: 10 }}>
                                    <Icon
                                        name='account-heart'
                                        type='material-community'
                                        color='#333333'
                                        size={36}
                                    />
                                </View>
                                <View style={{ flex: 0.9, borderBottomColor: '#333333', borderBottomWidth: 1, alignSelf: 'center', padding: 10, marginRight: 10 }}>
                                    <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                        Lapak Favorit
                                    </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={{ flex: 0.15 }}>
                        <TouchableHighlight style={{ flex: 1 }} underlayColor="#F0F8FF" onPress={() => Alert.alert("Fitur ini masih sedang dalam tahap pengembangan")}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.1, padding: 10 }}>
                                    <Icon
                                        name='help-circle'
                                        type='feather'
                                        color='#333333'
                                        size={32}
                                    />
                                </View>
                                <View style={{ flex: 0.9, borderBottomColor: '#333333', borderBottomWidth: 1, alignSelf: 'center', padding: 10, marginRight: 10 }}>
                                    <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                        Bantuan
                                    </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={{ flex: 0.15 }}>
                        <TouchableHighlight style={{ flex: 1 }} underlayColor="#F0F8FF" onPress={() => Alert.alert("Fitur ini masih sedang dalam tahap pengembangan")}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.1, padding: 10 }}>
                                    <Icon
                                        name='phone'
                                        type='material-community'
                                        color='#333333'
                                        size={36}
                                    />
                                </View>
                                <View style={{ flex: 0.9, borderBottomColor: '#333333', borderBottomWidth: 1, alignSelf: 'center', padding: 10, marginRight: 10 }}>
                                    <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                        Hubungi Tim Pengembang
                                    </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                    <View style={{ flex: 0.15 }}>
                        <TouchableHighlight style={{ flex: 1 }} underlayColor="#F0F8FF" onPress={() => Alert.alert("Fitur ini masih sedang dalam tahap pengembangan")}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 0.1, padding: 10 }}>
                                    <Icon
                                        name='settings'
                                        type='material-community'
                                        color='#333333'
                                        size={32}
                                    />
                                </View>
                                <View style={{ flex: 0.9, borderBottomColor: '#333333', borderBottomWidth: 1, alignSelf: 'center', padding: 10, marginRight: 10 }}>
                                    <Text style={{ color: '#333333', fontFamily: 'montserrat-regular', fontSize: 20 }}>
                                        Pengaturan Akun
                                    </Text>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
};