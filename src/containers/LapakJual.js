import React, { Component } from "react";
import {
	StyleSheet,
	Text,
	View,
	ListView,
	FlatList,
	TouchableHighlight
} from "react-native";
import ActionButton from 'react-native-action-button';
import {StackNavigator} from 'react-navigation';
import ProductsItem from "../components/ProductsItem";
import { Icon } from 'react-native-elements';
import firebase from "../firebase.js";

var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class Lapak extends Component {
	static navigationOptions = {
		headerTitle: 'Lapak',
    	headerStyle: {
      		backgroundColor: '#1e90ff',
    	},
    	headerTintColor: '#fff',
    	headerTitleStyle: {
      		fontWeight: "normal",
      		fontSize: 20,
      		fontFamily: 'Helvetica-Bold'
    	},
	};

	constructor(props) {
		super(props);

		this.productsRef = firebase.database().ref().child("beranda");
		this.state = {
			productsSource: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 != row2}),
		};
		this.products = [];
	}

	componentDidMount() {
        this.productsRef.orderByChild('userId').equalTo(uniqueId).on("child_added", (dataSnapshot) => {
			if(dataSnapshot.val().tipeLapak == 'Jual'){
				this.products.push({id: dataSnapshot.key, product: dataSnapshot.val()});
				this.setState({
					productsSource: this.state.productsSource.cloneWithRows(this.products)
				});
			}
			this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
		});

		this.productsRef.orderByChild('userId').equalTo(uniqueId).on("child_changed", (dataSnapshot) => {
			if(dataSnapshot.val().tipeLapak == 'Jual'){
				this.products = this.products.filter((x) => x.id != dataSnapshot.key);
				this.products.push({id: dataSnapshot.key, product: dataSnapshot.val()});
				this.setState({
					productsSource: this.state.productsSource.cloneWithRows(this.products)
				});
			}
			this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
		});

		this.productsRef.orderByChild('userId').equalTo(uniqueId).on("child_removed", (dataSnapshot) => {
			if(dataSnapshot.val().tipeLapak == 'Jual'){
				this.products = this.products.filter((x) => x.id != dataSnapshot.key);
				this.setState({
					productsSource: this.state.productsSource.cloneWithRows(this.products)
				});
			}
			this.products = this.products.sort(function(a,b) {return (b.product.timestamp - a.product.timestamp) }); 
		});
	}

	componentWillUnmount() {
		this.productsRef.off();
	}
	render() {
		return (
			<View style={styles.container}>
				<FlatList
					data={this.products}
					keyExtractor={(item, index) => item.id}
					renderItem={({item, index}) => (
						<TouchableHighlight 
							underlayColor="#D3D3D3"
							onPress={() => {
								this.props.navigation.navigate('DetailsJual', {
									product: item.product,
								});
							}}
							style={styles.button}>
							<ProductsItem index={index} products={item.product} />
						</TouchableHighlight>
					)}
				/>
				<ActionButton buttonColor="#87ceeb">
					<ActionButton.Item buttonColor='#87ceeb' title="Buat Lapak" onPress={() => this.props.navigation.navigate('MenuBuatLapak')}>
						<Icon name='create' size={25} color="#ffffff"/>
					</ActionButton.Item>
				</ActionButton>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#F0F8FF"
	},
	button: {
		alignItems: "center",
		justifyContent: "center",
		paddingRight: 10,
		paddingLeft: 10
	},
});
