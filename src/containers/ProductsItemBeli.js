import React from "react";
import { Icon } from 'react-native-elements';
import {
	StyleSheet,
	Text,
	View,
	Image,
	TouchableHighlight
} from "react-native";

const ProductsItem = ({ products, index }) => {

	let number = (index + 1).toString();
	var months = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
	var a = new Date(products.timestamp * 1000);
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes() + "";
	var price = products.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
	if(min.length == 1) {
		min = "0" + min;
	}
	var time = date + " " + month + " " + year + ", " + hour + ":" + min;
	var lapak_type = products.tipeLapak;
	var price = "Rp " + price + " / " + products.satuanIkan;

	return (
		<View style={styles.products_item}>
			<View style={styles.products_photo}>
				{ products.imageURL != undefined ? <Image style={styles.photo} source={{ uri: products.imageURL }} />
				: <Image source={require('../assets/icons/fish_icon.png')} style={[styles.photo]}></Image>}
			</View>
			<View style={styles.products_text}>
				<View style={styles.text_container}>
					<View style={styles.subtext_container}>
					<View style={styles.button}>
                        <TouchableHighlight
                            style={styles.type_flag}
                        >
                            <Text style={styles.buttonText}>{lapak_type}</Text>
                        </TouchableHighlight>
                    </View>
					<Text style={styles.title}>{products.judul}</Text>
					{ products.tipeLapak == "Jual" ? 
					<View>
					<Text style={styles.details}>{time}</Text>
					</View>:
					<View>
					<Text style={styles.details}>{time}</Text>
					</View>}
					</View>
					<Text style={styles.details}>{products.location}</Text>
					<Text style={styles.price}>{price}</Text>
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	products_item: {
		flex: 1,
		flexDirection: "row",
		padding: 5,
		borderBottomWidth: 1.5,
		borderBottomColor: "#d3d3d3"
	},
	products_text: {
		flex: 2,
		flexDirection: "row"
	},
	text_container: {
		marginLeft: 10,
		flex: 3,
		flexDirection: 'column',
	},
	subtext_container: {
		flex: 0.85,
	},
	title: {
		fontSize: 16,
		color: "#333333",
		fontFamily: "Montserrat-Bold",
		marginBottom: 3,
	},
	price: {
		fontSize: 16,
		flex: 0.15,
		color:"#FF0033",
		fontFamily: "Montserrat-Regular",
	},
	details: {
		fontSize: 14,
		color:"#999999",
		fontFamily: "Montserrat-Regular"
	},
	products_photo: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	photo: {
		width: 120,
		height: 120
	},
	icon: {
		width: 120,
		height: 120,
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	},
	button: {
        flex: 0.1,
	},
	type_flag: {
        alignItems: 'center',
        backgroundColor: '#1e90ff',
        width: 40,
        height: 16,
        borderRadius: 2
    },
    buttonText: {
        fontFamily: 'Montserrat-Bold',
        fontSize: 14,
        color: 'white',
        flex: 1,
    },
});

export default ProductsItem;
