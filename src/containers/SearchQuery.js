import React, { Component } from 'react';
import { AppRegistry, View, Text, TouchableHighlight, StyleSheet, ListView } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { SearchBar } from 'react-native-elements';
import ProductsItem from "../components/ProductsItem";

import firebase from "../firebase.js";
export default class SearchQuery extends Component {
    constructor(props) {
        super(props);
        this.productsRef = firebase.database().ref("beranda/");
        this.state = {
            productsSource: new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 != row2 }),
        };
    }

    componentWillUnmount() {
        this.productsRef.off();
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <SearchBar
                    lightTheme
                    placeholder='Search'
                    placeholderTextColor='#999999'
                    inputStyle={{
                        color: '#333333',
                        backgroundColor: '#ffffff',
                        fontFamily: 'Helvetica-Regular',
                        fontSize: 14
                    }}
                    round
                    returnKeyType='search'
                    containerStyle={{ backgroundColor: '#1e90ff' }}
                    onChangeText={(text) => this.setState({ searchText: text })}
                    onSubmitEditing={() => this.searchProduct()}
                    clearIcon />
                <ListView dataSource={this.state.productsSource} renderRow={this._renderItem.bind(this)} enableEmptySections={true} />
            </View>
        );
    };

    nav(tipeLapak, products) {
		if(tipeLapak == "Jual") {
			this.props.navigation.navigate('DetailsJual', {
				product: products.content,
			});
		} else {
			this.props.navigation.navigate('DetailsBeli', {
				product: products.content,
			});
		}
    }
    
    _renderItem(products) {
        return (
            <TouchableHighlight
                underlayColor="#D3D3D3"
                onPress={() => this.nav(products.content.tipeLapak, products)}
                style={styles.button}>
                <ProductsItem products={products.content} />
            </TouchableHighlight>
        );
    }

    searchProduct() {
        var searchText = this.state.searchText.toString().toLowerCase();
        this.productsRef.orderByChild('namaIkan').startAt(searchText).endAt(searchText+'\uf8ff').on('value', (data) => {
            var products = [];
            data.forEach((child) => {
                products.push({
                    content: child.val(),
                });
            })
            products = products.sort(function(a,b) {return (b.content.timestamp - a.content.timestamp) }); 
            this.setState({
                productsSource: this.state.productsSource.cloneWithRows(products)
            });
        });
    }
};

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        justifyContent: "center",
        paddingRight: 10,
        paddingLeft: 10
    },
});
