import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, TextInput, Image, Button, Alert, TouchableOpacity, Picker, TouchableHighlight, ActivityIndicator, BackHandler, Dimensions} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'react-native-fetch-blob';
import DatePicker from 'react-native-datepicker'
import { StackNavigator, NavigationActions } from 'react-navigation';
import AlertUI from 'react-native-awesome-alerts';
import firebase from '../firebase.js'
import Modal from "react-native-modalbox";
import Spinner from 'react-native-loading-spinner-overlay';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import listIkan from './listIkan'
import { buatLapakBeliStyles } from '../assets/stylesheet/BuatLapak';

var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();
var storageRef = firebase.storage().ref();

export default class BuatLapakJual extends Component {
	render() {
		return (
			<View style={buatLapakBeliStyles.container}>
				<View style={{flex:10}}>
					<ScrollView>
						<View style={[buatLapakBeliStyles.content]}>
							<View style={[buatLapakBeliStyles.imageContainer]}>
								<TouchableOpacity id="picker" onPress={() => {this.imgPicker(1)}}>
									<View>
									{ (this.state.imageSource1 == null || this.state.imageSource1.uri == null)? <Image source={require('../assets/icons/add-image.png')} style={[buatLapakBeliStyles.image]}></Image>:
									<Image style={[buatLapakBeliStyles.image]} source={this.state.imageSource1} />
									}
									</View>
								</TouchableOpacity>
							</View>
							<Text style={[buatLapakBeliStyles.contentText]}>Judul Iklan</Text>
							<TextInput 
								id="title"
								style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
								onChangeText={text => this.setState({title: text})}>
							</TextInput>
							<Text style={[buatLapakBeliStyles.contentText]}>Lokasi Penangkapan</Text>
							<TextInput 
								id="title"
								style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
								onChangeText={text => this.setState({location: text})}>
							</TextInput>
							<Text style={[buatLapakBeliStyles.contentText]}>Jenis Pengolahan Ikan</Text>
							<Picker
								id="pick"
								selectedValue={this.state.jenisIkan}
								onValueChange={(itemValue, itemIndex) => this.setState({jenisIkan: itemValue})}>
								<Picker.Item label="Pilih jenis pengolahan produk" value="" />
								<Picker.Item label="Ikan Segar" value="Segar" />
								<Picker.Item label="Ikan Olahan" value="Olahan" />
							</Picker>
							{ (this.state.jenisIkan == "Olahan")? 
								<View>
									<Text style={[buatLapakBeliStyles.contentText]}>Cara Pengolahan</Text>
									<Picker
										id="pick"
										selectedValue={this.state.olahan}
										onValueChange={(itemValue, itemIndex) => this.setState({olahan: itemValue})}>
										<Picker.Item label="Pilih cara pengolahan" value="" />
										<Picker.Item label="Dikeringkan" value="Dikeringkan" />
										<Picker.Item label="Diasinkan" value="Diasinkan" />
										<Picker.Item label="Diasapkan" value="Diasapkan" />
										<Picker.Item label="Di-fillet" value="Di-fillet" />
										<Picker.Item label="Dikalengkan" value="Dikalengkan" />
										<Picker.Item label="Dikemas Vakum" value="Divakum" />
									</Picker>
								</View>:
								<View></View>
							}
							<Text style={[buatLapakBeliStyles.contentText]}>Nama Ikan</Text>
							{/* <Picker
								selectedValue={this.state.namaIkan}
								onValueChange={(itemValue, itemIndex) => this.setState({namaIkan: itemValue})}>
								<Picker.Item label="Pilih nama Ikan" value="" />
								<Picker.Item label="Ikan Tuna" value="tuna" />
								<Picker.Item label="Ikan Tongkol" value="tongkol" />
								<Picker.Item label="Ikan Kakap" value="kakap" />
								<Picker.Item label="Ikan Lele" value="lele" />
								<Picker.Item label="Ikan Salmon" value="salmon" />
							</Picker> */}
							<TouchableOpacity style={[buatLapakBeliStyles.pickerListIkan]} onPress={this.onShowListIkan}>
								<Text style={[buatLapakBeliStyles.pickerTextListIkan]}>{this.state.namaIkan}</Text>
							</TouchableOpacity>
							<ModalFilterPicker
								title="Pilih Nama Ikan"
								noResultsText="Tidak ada nama Ikan"
								visible={this.state.visibleListIkan}
								onSelect={this.onSelectListIkan}
								onCancel={this.onCancelListIkan}
								options={listIkan}
								titleTextStyle={[buatLapakBeliStyles.titleTextStyleListIkan]}
								filterTextInputStyle={[buatLapakBeliStyles.filterTextInputStyleListIkan]}
								overlayStyle={[buatLapakBeliStyles.overlayStyleListIkan]}
								listContainerStyle={[buatLapakBeliStyles.listContainerStyleListIkan]}
								optionTextStyle={[buatLapakBeliStyles.optionTextStyleListIkan]}
							/>
							<Text style={[buatLapakBeliStyles.contentText]}>Pilih pembelian</Text>
							<Picker
								selectedValue={this.state.pembelian}
								onValueChange={(itemValue, itemIndex) => {this.gantiPembelian(itemValue)}}>
								<Picker.Item label="Berat" value="berat" />
								<Picker.Item label="Satuan" value="satuan" />
							</Picker>
							{ (this.state.pembelian == "satuan")? 
								<View>
									<Text style={[buatLapakBeliStyles.contentText]}>Jumlah
									{ " " + this.state.satuan }
									</Text>
									<View style={[buatLapakBeliStyles.picker2]}>
									<TextInput 
										id="weight"
										style={{flex:8,fontFamily: "Montserrat-Regular",color: "#666666"}}
										keyboardType='numeric'
										value={this.state.weight}
										onChangeText={text => this.setState({weight: this.onInputNumeric(text)})}>
									</TextInput>
									<Picker
										style={{flex:4}}
										selectedValue={this.state.satuanPicker}
										itemStyle={[buatLapakBeliStyles.contentText]}
										onValueChange={(itemValue, itemIndex) => this.setModalOpen(itemValue)}>
										<Picker.Item label="Ekor" value="Ekor" />
										<Picker.Item label="Buah" value="Buah" />
										<Picker.Item label="Kemasan" value="Kemasan" />
										<Picker.Item label="Lainnya" value="Lainnya" />
									</Picker>
									</View>
									<Text style={[buatLapakBeliStyles.contentText]}>Harga Ikan per 
									{ " " + this.state.satuan }
									</Text>
								</View>:
								<View>
									<Text style={[buatLapakBeliStyles.contentText]}>Jumlah dalam
									{ " " + this.state.satuan }
									</Text>
									<View style={[buatLapakBeliStyles.picker2]}>
									<TextInput 
										id="weight"
										style={{flex:8,fontFamily: "Montserrat-Regular",color: "#666666"}}
										keyboardType='numeric'
										value={this.state.weight}
										onChangeText={text => this.setState({weight: this.onInputNumeric(text)})}>
									</TextInput>
									<Picker
									style={{flex:4}}
										selectedValue={this.state.satuanPicker}
										itemStyle={[buatLapakBeliStyles.contentText]}
										onValueChange={(itemValue, itemIndex) => this.setModalOpen(itemValue)}>
										<Picker.Item label="Gram" value="Gram" />
										<Picker.Item label="Kilogram" value="Kilogram" />
										<Picker.Item label="Ons" value="Ons" />
										<Picker.Item label="Kuintal" value="Kuintal" />
										<Picker.Item label="Lainnya" value="Lainnya" />
									</Picker>
									</View>
									<Text style={[buatLapakBeliStyles.contentText]}>Harga Ikan per 
									{ " " + this.state.satuan }
									</Text>
								</View>
							}
							<TextInput 
								id="price"
								style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
								keyboardType='numeric'
								value={this.state.price}
								onChangeText={(text)=> this.setState({price : this.onInputNumeric(text)})}>
							</TextInput>
							<Text style={[buatLapakBeliStyles.contentText]}>Ukuran</Text>
							<Picker
								selectedValue={this.state.ukuranIkan}
								onValueChange={(itemValue, itemIndex) => this.setState({ukuranIkan: itemValue})}>
								<Picker.Item label="Pilih ukuran Ikan" value="" />
								<Picker.Item label="Besar" value="Besar" />
								<Picker.Item label="Sedang" value="Sedang" />
								<Picker.Item label="Kecil" value="Kecil" 
							/>
							</Picker>
							<Text style={[buatLapakBeliStyles.contentText]}>Deskripsi</Text>
							<TextInput 
								id="description"
								style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
								onChangeText={text => this.setState({description: text})}
								multiline = {true}>
							</TextInput>
							<Text style={[buatLapakBeliStyles.contentText]}>Waktu Ditangkap</Text>
							<DatePicker
								style={{width: 200, marginTop: 5, marginBottom: 20}}
								date={this.state.date}
								mode="datetime"
								placeholder={this.state.date}
								format="YYYY-MM-DD, HH:mm"
								is24Hour
								confirmBtnText="Confirm"
								cancelBtnText="Cancel"
								androidMode="spinner"
								customStyles={{
									dateIcon: {
										position: 'absolute',
										left: 0,
										top: 4,
										marginLeft: 0
									},
									dateInput: {
										marginLeft: 36
									},
									placeholderText: {
										fontSize: 12,
										color: '#666666'
									}
								}}
								onDateChange={(date) => {this.setState({date: date})}}
							/>
						</View>
					</ScrollView>
				</View>
				<View style={buatLapakBeliStyles.footer}>
					<TouchableHighlight id="add" style={buatLapakBeliStyles.buttonFooter} onPress={() => {this.showAlertConfirm()}}>
						<Text style={[buatLapakBeliStyles.textFooter]}>Submit</Text>
					</TouchableHighlight>
				</View>
				<AlertUI
					show={this.state.showAlertConfirm}
					showProgress={false}
					title="Konfimasi"
					message="Apa anda yakin akan menambahkan lapak jual?"
					closeOnTouchOutside={false}
					closeOnHardwareBackPress={false}
					showCancelButton={true}
					showConfirmButton={true}
					cancelText="Batal"
					confirmText="Tambah"
					confirmButtonColor="#1E90FF"
					cancelButtonColor="#999999"
					titleStyle={[buatLapakBeliStyles.alertUIText]}
					messageStyle={[buatLapakBeliStyles.alertUIText]}
					cancelButtonTextStyle={[buatLapakBeliStyles.alertUIText]}
					confirmButtonTextStyle={[buatLapakBeliStyles.alertUIText]}
					onCancelPressed={() => {
						this.hideAlertConfirm();
					}}
					onConfirmPressed={() => {
						this.hideAlertConfirm();
						this.addToFirebase();
					}}
				/>
				<AlertUI
					show={this.state.showAlertError}
					showProgress={false}
					title="Error"
					message={this.state.valid}
					closeOnTouchOutside={false}
					closeOnHardwareBackPress={false}
					showCancelButton={true}
					showConfirmButton={false}
					cancelText="Tutup"
					cancelButtonColor="#999999"
					titleStyle={[buatLapakBeliStyles.alertUIText]}
					messageStyle={[buatLapakBeliStyles.alertUIText]}
					cancelButtonTextStyle={[buatLapakBeliStyles.alertUIText]}
					onCancelPressed={() => {
						this.hideAlertError();
					}}
				/>
				<AlertUI
					show={this.state.showAlertBack}
					title="Apa anda yakin untuk kembali?"
					closeOnTouchOutside={false}
					closeOnHardwareBackPress={false}
					showCancelButton={true}
					showConfirmButton={true}
					cancelText="Tidak"
					confirmText="Ya"					
					cancelButtonColor="#999999"
					confirmButtonColor="#1E90FF"
					titleStyle={[buatLapakBeliStyles.alertUIText]}
					messageStyle={[buatLapakBeliStyles.alertUIText]}
					cancelButtonTextStyle={[buatLapakBeliStyles.alertUIText]}
					confirmButtonTextStyle={[buatLapakBeliStyles.alertUIText]}
					onCancelPressed={() => {
						this.hideAlertBack();
					}}
					onConfirmPressed={() => {
						this.confirmBack();
					}}
				/>
				<Modal style={[buatLapakBeliStyles.modal]} 
					position={"center"} 
					isOpen={this.state.modalVisible}
					swipeToClose={false}
					backdropPressToClose={false}>
					<View style={[buatLapakBeliStyles.contentModal]}>
						<Text style={buatLapakBeliStyles.contentTextModal}>Masukkan Pilihan Lain Anda</Text>
						<TextInput
						id="modal"
						style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
						value={this.state.modalText}
						onChangeText={(text)=> this.setState({modalText : text})}/>
						</View>
						<TouchableOpacity onPress={() => this.setState({modalVisible: !this.state.modalVisible, satuan: this.state.modalText})}>
							<View style={[buatLapakBeliStyles.btn]}>
								<Text style={[buatLapakBeliStyles.buttonText]}>OK</Text>
							</View>
						</TouchableOpacity>
				</Modal>
				<Spinner visible={this.state.modalLoad} color='#1E90FF' overlayColor={'rgba(240, 248, 255, 1)'}/>
			</View>
		);
	}

	// componentDidMount() {
	// 	BackHandler.addEventListener('hardwareBackPress', this.onBackClicked);
	// }

	// componentWillUnmount() {
	// 	BackHandler.removeEventListener("hardwareBackPress", this.onBackClicked);
	// }

	_onBackClicked = () => {
    if(!this.state.showAlertConfirm && !this.state.showAlertError && !this.state.modalVisible) {
			this.showAlertBack()
			return true;
		}
    return false;
	} 
	
	confirmBack = () => {
		this.props.navigation.goBack(null);
	}

	onBackButtonPressAndroid = () => {
		if(!this.state.showAlertConfirm && !this.state.showAlertError && !this.state.modalVisible) {
			this.showAlertBack()
			return true;
		}
    return false;
  };

	onShowListIkan = () => {
    this.setState({ visibleListIkan: true });
  }

  onSelectListIkan = (picked) => {
    this.setState({
      namaIkan: picked,
      visibleListIkan: false
    })
  }

  onCancelListIkan = () => {
    this.setState({
      visibleListIkan: false
    });
	}
	
	gantiPembelian(itemValue) {
		this.setState({pembelian: itemValue})
		if(itemValue == "Berat") {
			this.setState({satuanPicker: "Gram"})
		} else {
			this.setState({satuanPicker: "Ekor"})
		}
	}
	
	setModalOpen(itemVal) {
		if(itemVal == "Lainnya"){
			this.setState({modalVisible: true});
			this.setState({satuanPicker : itemVal});
			this.setState({modalText : ''})
		} else {
			this.setState({satuanPicker : itemVal});
			this.setState({satuan : itemVal});
		}
	}
	setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

	showAlertConfirm = () => {
		this.setState({
		  showAlertConfirm: true
		});
	};
	
	hideAlertConfirm = () => {
		this.setState({
			showAlertConfirm: false
		});
	};

	showAlertError = () => {
		this.setState({
		  showAlertError: true
		});
	};

	showAlertBack = () => {
		this.setState({
		  showAlertBack: true
		});
	};

	hideAlertBack = () => {
		this.setState({
		  showAlertBack: false
		});
	};
	
	hideAlertError = () => {
		this.setState({
			showAlertError: false
		});
	};

	onInputNumeric (text) {
		text = text.replace(/[^0-9]/g, '')
		return text.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
	}

	changeToNumeric(text) {
		text = text.replace(/[^0-9]/g, '')
		return Number(text)
	}
	
	constructor(props) {
		super(props);
		this.onBackClicked = this._onBackClicked.bind(this);
		this.berandaRef = firebase.database().ref().child('beranda');
		this.detailRef = firebase.database().ref().child('productDetails');
		this.userRef = firebase.database().ref('users/' + uniqueId);
		let dateNow = new Date()
		date = dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1) + "-" + dateNow.getDate() + ", " + dateNow.getHours() + ":" + dateNow.getMinutes()
		this.state = {
		  imageSource1: null,
		  location: 'jakarta',
		  weight: '',
		  price: '',
		  title: '',
		  description: '',
		  timestamp: '',
		  isImage1: false,
		  namaIkan: 'Pilih nama ikan',
		  jenisIkan: '',
		  kondisiIkan: '',
		  ukuranIkan: '',
		  date: date,
		  satuan: 'Gram',
		  showAlertConfirm: false,
			showAlertError: false,
			showAlertBack: false,
			modalVisible: false,
			modalText: '',
			modalLoad: false,
			visibleListIkan: false,
		};
		this.imgPicker = this.imgPicker.bind(this);
	}

	addToFirebase() {
		let valid = this.validateForm(this.state)
		this.setState({valid: valid})
		if(valid != ''){
			this.showAlertError()
		} else {
			
			if(this.state.jenisIkan == "Olahan") {
				this.state.kondisiIkan = this.state.jenisIkan + " dengan cara " + this.state.olahan
			} else {
				this.state.kondisiIkan = this.state.jenisIkan
				this.state.olahan = "Segar"
			}
			
			var newPostDetail = this.detailRef.push({
				description: this.state.description,
				weight: this.changeToNumeric(this.state.weight),
				ukuran: this.state.ukuranIkan,
				jenisProduk: this.state.jenisIkan,
				kondisiIkan: this.state.kondisiIkan,
				caraOlah: this.state.olahan,
				userId: uniqueId,
			});

			

			// this.userRef.child(newPostRef.key).push({
			// 	postId: newPostRef.key
			// });

			this.addImage(newPostDetail.key)
		}
	}

	addImage(key) {
		return new Promise((resolve, reject) => {
			const Blob = RNFetchBlob.polyfill.Blob
			const fs = RNFetchBlob.fs
			window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
			window.Blob = Blob
			let uploadBlob = null
			var storageRef = firebase.storage().ref();
			var imageRef = storageRef.child('postImages/' + key + '/1.jpg');
			Blob.build(RNFetchBlob.wrap(this.state.imagePath1), { type : 'image/jpeg' })
			.then((blob) => {
				uploadBlob = blob
				this.setState({modalLoad: true});
				return imageRef.put(blob, { contentType : 'image/png' })
			})
			.then((snapshot) => { 
				uploadBlob.close()
				return imageRef.getDownloadURL()
			})
			.then((url) => {
				// this.berandaRef.child(key).update({
				// 	imageURL: url
				// })
				let time = this.convertDateToTimestamp(this.state.date);
				var newPostRef = this.berandaRef.push({
					location: this.state.location,
					price: this.changeToNumeric(this.state.price),
					judul: this.state.title,
					timestamp: Number(time),
					productId: key,
					namaIkan: this.state.namaIkan.toString().toLowerCase(),
					satuanIkan: this.state.satuan,
					userId: uniqueId,
					tipeLapak: "Jual",
					imageURL: url
				});
				this.setState({modalLoad: false});
				this.props.navigation.dispatch(NavigationActions.back())
				resolve(url)
			})
			.catch((error) =>{
				reject(error)
			})
		})
	}
	
	convertDateToTimestamp(timestamp){
		let arrayDate = timestamp.split("-")
		let arrayHours = arrayDate[2].split(":")
		var date = new Date(arrayDate[0],arrayDate[1] - 1,arrayDate[2].substring(0,2),arrayHours[0].slice(-2),arrayHours[1],0 ,0)
		timestampDate = date.getTime() + ""
		let time = "" + timestampDate.substring(0, 10)
		return time
	}
	
	validateForm(state){
		let strValid = "";
		if(state.title == ''){
			strValid = "Isi judul dari ikan \n";
		} else if(state.imageSource1 == null){
			strValid = "Pilih minimal 1 gambar \n";
		} else if(state.jenisIkan == ''){
			strValid = "Isi jenis pengolahan produk \n";
		} else if(state.jenisIkan == 'Olahan' && state.olahan == ''){
			strValid = "Isi cara pengolahan ikan \n";
		} else if(state.namaIkan == 'Pilih nama ikan'){
			strValid = "Isi nama dari ikan \n";
		} else if(state.weight == ''){
			strValid = "Isi jumlah/bobot dari ikan \n";
		} else if(state.price == ''){
			strValid = "Isi harga dari ikan \n";
		} else if(state.ukuranIkan == ''){
			strValid = "Isi ukuran dari ikan \n";
		} else if(state.description == ''){
			strValid = "Isi deskripsi dari ikan \n";
		} else if(state.date == ''){
			strValid = "Isi tanggal ikan ditangkap \n";
		} else if(state.location == ''){
			strValid = "Isi lokasi ikan ditangkap \n";
		}
		return strValid
	}
	
	imgPicker(imgNumber){

		const options = {
			quality: 1.0,
			maxWidth: 500,
			maxHeight: 500,
			storageOptions: {
				skipBackup: true
			},
			title: null,
			takePhotoButtonTitle: 'Tambah gambar dari kamera',
			chooseFromLibraryButtonTitle: 'Pilih gambar dari galeri'
		};

		ImagePicker.showImagePicker(options, (response) => {
			let source = { uri: response.uri };
			this.setState({
				isImage1: true,
				imageSource1: source,
				imagePath1: response.path
			});
		});
	}
}
