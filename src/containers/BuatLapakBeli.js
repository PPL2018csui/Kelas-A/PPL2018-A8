import React, { Component } from "react";
import { StyleSheet, View, Text, ScrollView, TextInput, Image, Button, Alert, TouchableOpacity, Picker, TouchableHighlight, KeyboardAvoidingView,Animated, Keyboard ,Dimensions} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { StackNavigator } from 'react-navigation';
import { CheckBox } from 'react-native-elements';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import listIkan from './listIkan';
import Modal from "react-native-modalbox";
import Spinner from 'react-native-loading-spinner-overlay';
import DatePicker from 'react-native-datepicker'
import AlertUI from 'react-native-awesome-alerts';
var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();
const { width, height } = Dimensions.get('window');
import firebase from "../firebase.js";
var storageRef = firebase.storage().ref();

export default class BuatLapakJual extends Component {
	render() {
		return (
			<View style={styles.container}>
				<View style={{flex:10}}>
				<ScrollView>
					<View style={[styles.content]}>
						
						<Text style={[styles.contentText]}>Judul Iklan</Text>
						<TextInput 
							onChangeText={text => this.setState({title: text})}
							// onPress={() => {this.imgPicker(1)}}
							>
						</TextInput>
						<Text style={[styles.contentText]}>Lokasi Penangkapan</Text>
						<TextInput 
							id="title"
							style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
							onChangeText={text => this.setState({location: text})}>
						</TextInput>
						<Text style={[styles.contentText]}>Jenis Ikan</Text>
						<Picker
							selectedValue={this.state.jenisIkan}
							onValueChange={(itemValue, itemIndex) => this.setState({jenisIkan: itemValue})}>
							<Picker.Item label="Pilih jenis Ikan" value="" />
							<Picker.Item label="Ikan Segar" value="Segar" />
							<Picker.Item label="Ikan Olahan" value="Olahan" />
						</Picker>
						
						
						<ModalFilterPicker
						  title="Pilih Nama Ikan"
						  noResultsText="Tidak ada nama Ikan"
						  visible={this.state.visibleListIkan}
						  onSelect={this.onSelectListIkan}
						  onCancel={this.onCancelListIkan}
						  options={listIkan}
						  titleTextStyle={[styles.titleTextStyleListIkan]}
						  filterTextInputStyle={[styles.filterTextInputStyleListIkan]}
						  overlayStyle={[styles.overlayStyleListIkan]}
						  listContainerStyle={[styles.listContainerStyleListIkan]}
						  optionTextStyle={[styles.optionTextStyleListIkan]}>
						</ModalFilterPicker>
						{ (this.state.jenisIkan == "Olahan")? 
						<View>
						<Text style={[styles.contentText]}>Jenis Pengolahan</Text>	
						<CheckBox title='Dikeringkan' checked={this.state.checked1} onPress={() => this.setState({checked1: !this.state.checked1})} />
						<CheckBox title='Diasinkan' checked={this.state.checked2} onPress={() => this.setState({checked2: !this.state.checked2})} />
						<CheckBox title='Diasapkan' checked={this.state.checked3} onPress={() => this.setState({checked3: !this.state.checked3})} />
						<CheckBox title='Difillet' checked={this.state.checked4} onPress={() => this.setState({checked4: !this.state.checked4})} />
						<CheckBox title='Dikalengkan' checked={this.state.checked5} onPress={() => this.setState({checked5: !this.state.checked5})} />
						<CheckBox title='Dikemas vakum' checked={this.state.checked6} onPress={() => this.setState({checked6: !this.state.checked6})} />
						<TextInput>Lainnya: </TextInput>
						</View>
						:<View></View> }
						<Text style={[styles.contentText]}>Nama Ikan</Text>
						<TouchableOpacity style={[styles.pickerListIkan]} onPress={this.onShowListIkan}>
						  <Text style={[styles.pickerTextListIkan]}>{this.state.namaIkan}</Text>
						</TouchableOpacity>
						<Text style={[styles.contentText]}>Pilih Satuan Pembelian</Text>
						<Picker
							selectedValue={this.state.pembelian}
							onValueChange={(itemValue, itemIndex) => this.setState({pembelian: itemValue})}>
							<Picker.Item label="Berat" value="Berat" />
							<Picker.Item label="Satuan" value="Satuan" />
						</Picker>

						{ (this.state.pembelian == "Satuan")? 
							<View>
								<Text style={[styles.contentText]}>Jumlah
								{ (this.state.satuan) }
								</Text>
								<View style={[styles.picker2]}>
								<TextInput
									id="weight"
									style={{flex:1, fontFamily: "Montserrat-Regular", color: "#666666", width: 200}}
									keyboardType='numeric'
									value={this.state.weight}
									onChangeText={text => this.setState({weight: this.onInputNumeric(text)})}>
								</TextInput>
								<Picker
									style={{flex:3}}
									selectedValue={this.state.satuan}
									itemStyle={[styles.contentText]}
									onValueChange={(itemValue, itemIndex) => this.setState({satuan: itemValue})}>
									<Picker.Item label="Ekor" value="Ekor" />
									<Picker.Item label="Buah" value="Buah" />
									<Picker.Item label="Kemasan" value="Kemasan" />
								</Picker>
								</View>
								<Text style={[styles.contentText]}>Harga yang diinginkan per
								{ ( " " + this.state.satuan ) }
								</Text>
								<TextInput 
									id="price"
									style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
									keyboardType='numeric'
									value={this.state.price}
									onChangeText={(text)=> this.setState({price : this.onInputNumeric(text)})}>
								</TextInput>
							</View>:
							<View>
								<Text style={[styles.contentText]}>Jumlah
								{ (" " + this.state.satuan )}
								</Text>
								<View style={[styles.picker2]}>
								<TextInput 
									id="weight"
									style={{flex:1,fontFamily: "Montserrat-Regular",color: "#666666"}}
									keyboardType='numeric'
									value={this.state.weight}
									onChangeText={text => this.setState({weight: this.onInputNumeric(text)})}>
								</TextInput>
								<Picker
									style={{flex:3}}
									selectedValue={this.state.satuan}
									itemStyle={[styles.contentText]}
									onValueChange={(itemValue, itemIndex) => this.setState({satuan: itemValue})}>
									<Picker.Item label="Gram" value="Gram" />
									<Picker.Item label="Kilogram" value="Kilogram" />
									<Picker.Item label="Ons" value="Ons" />
									<Picker.Item label="Kuintal" value="Kuintal" />
								</Picker>
								</View>
								
								<Text style={[styles.contentText]}>Harga yang diinginkan per 
								{ (" " + this.state.satuan )}
								</Text>		
								<TextInput 
									id="price"
									style={{fontFamily: "Montserrat-Regular",color: "#666666"}}
									keyboardType='numeric'
									value={this.state.price}
									onChangeText={(text)=> this.setState({price : this.onInputNumeric(text)})}>
								</TextInput>
							</View>
						}

						{/* <Text style={[styles.contentText]}>Tampilkan total harga pada beranda?</Text>	
						<CheckBox title='Ya' checked={this.state.checked7} onPress={() => this.setState({checked7: !this.state.checked7})} /> */}
						
						<Text style={[styles.contentText]}>Pilih Metode Pembayaran</Text>
						<Picker
							selectedValue={this.state.pembayaran}
							onValueChange={(itemValue, itemIndex) => this.setState({pembayaran: itemValue})}>
							<Picker.Item label="Pilih pembayaran" value="" />
							<Picker.Item label="Tunai" value="Tunai" />
							<Picker.Item label="Transfer Bank" value="Transfer Bank" />
						</Picker>
					</View>
				</ScrollView>
				</View>
				<View style={styles.footer}>
				<TouchableHighlight style={styles.buttonFooter} onPress={() => {this.showAlertConfirm()}}>
					<Text style={[styles.textFooter]}>Submit</Text>
				</TouchableHighlight>
				</View>
				<AlertUI
					show={this.state.showAlertConfirm}
					showProgress={false}
					title="Konfimasi"
					message="Apa anda yakin akan menambahkan lapak jual?"
					closeOnTouchOutside={false}
					closeOnHardwareBackPress={false}
					showCancelButton={true}
					showConfirmButton={true}
					cancelText="Batal"
					confirmText="Tambah"
					confirmButtonColor="#1E90FF"
					cancelButtonColor="#999999"
					titleStyle={[styles.alertUIText]}
					messageStyle={[styles.alertUIText]}
					cancelButtonTextStyle={[styles.alertUIText]}
					confirmButtonTextStyle={[styles.alertUIText]}
					onCancelPressed={() => {
						this.hideAlertConfirm();
					}}
					onConfirmPressed={() => {
						this.hideAlertConfirm();
						this.addTodo();
					}}
				/>
				<AlertUI
					show={this.state.showAlertError}
					showProgress={false}
					title="Error"
					message={this.state.valid}
					closeOnTouchOutside={false}
					closeOnHardwareBackPress={false}
					showCancelButton={true}
					showConfirmButton={false}
					cancelText="Tutup"
					cancelButtonColor="#999999"
					titleStyle={[styles.alertUIText]}
					messageStyle={[styles.alertUIText]}
					cancelButtonTextStyle={[styles.alertUIText]}
					onCancelPressed={() => {
						this.hideAlertError();
					}}
				/>
			</View>
		);
	}

	constructor(props) {
		console.ignoredYellowBox = [
		  'Setting a timer'
		];
		super(props);
		this.berandaRef = firebase.database().ref().child('beranda');
		this.detailRef = firebase.database().ref().child('productDetails');
		this.userRef = firebase.database().ref('users/' + uniqueId);
		this.state = {
		  imageSource1: null,
		  location: '',
		  weight: '',
		  price: '',
		  title: '',
		  berat: '',
		  description: '',
		  timestamp: '',
		  length: '',
		  isImage1: false,
		  namaIkan: 'Pilih nama ikan',
		  jenisIkan: '',
		  kondisiIkan: '',
		  ukuranIkan: '',
			visibleListIkan: false,
			satuan: 'Gram'
		};
		// this.imgPicker = this.imgPicker.bind(this);
	}

	addOlahan() {
		let olahan = ""
		if (this.state.checked1) {
			olahan += "Dikeringkan, "
		}
		if (this.state.checked2) {
			olahan += "Diasinkan, "
		}
		if (this.state.checked3) {
			olahan += "Diasapkan, "
		}
		if (this.state.checked4) {
			olahan += "Difillet, "
		}
		if (this.state.checked5) {
			olahan += "Dikalengkan, "
		}
		if (this.state.checked6) {
			olahan += "Dikemas vakum, "
		}
		return olahan
	}
	
	addTodo() {
		let valid = this.validateForm(this.state)
		this.setState({valid: valid})
		if (valid != '') {
		  this.showAlertError();
		} else {
			let date = Date.now();
			date = Number(date.toString().slice(0, -3))
			let olahan = this.addOlahan().slice(0, -2);
			this.state.olahan = olahan
		  if(this.state.jenisIkan == "Olahan") {
				this.state.kondisiIkan = this.state.jenisIkan + " dengan cara " + this.state.olahan
		  } else {
				this.state.kondisiIkan = this.state.jenisIkan
				this.state.olahan = "Segar"
		  }
		  var newPostDetail = this.detailRef.push({
			weight: this.changeToNumeric(this.state.weight),
			userId: uniqueId,
			kondisiIkan: this.state.kondisiIkan,
			caraOlah: this.state.olahan,
			jenisProduk: this.state.jenisIkan,
			pembayaran: this.state.pembayaran,
			});
	
		  var newPostRef = this.berandaRef.push({
			location: this.state.location,
			judul: this.state.title,
			namaIkan: this.state.namaIkan.toString().toLowerCase(),
			price: this.changeToNumeric(this.state.price),
			tipeLapak: "Beli",
			timestamp: Number(date),
			satuanIkan: this.state.satuan,
			productId: newPostDetail.key,
			userId: uniqueId,
		  });
	
		  // this.userRef.push({
			// postId: newPostRef.key
		  // });

			Alert.alert("Tangkapan anda sudah berhasil disimpan!");
			this.props.navigation.goBack(null)
		}
	}

	showAlertConfirm = () => {
		this.setState({
		  showAlertConfirm: true
		});
	};
	
	hideAlertConfirm = () => {
		this.setState({
			showAlertConfirm: false
		});
	};

	showAlertError = () => {
		this.setState({
		  showAlertError: true
		});
	};

	showAlertBack = () => {
		this.setState({
		  showAlertBack: true
		});
	};

	hideAlertBack = () => {
		this.setState({
		  showAlertBack: false
		});
	};
	
	hideAlertError = () => {
		this.setState({
			showAlertError: false
		});
	};

	onShowListIkan = () => {
	  this.setState({ visibleListIkan: true });
	}

	onSelectListIkan = (picked) => {
	  this.setState({
	    namaIkan: picked,
	    visibleListIkan: false
	  })
	}

	onCancelListIkan = () => {
	  this.setState({
	    visibleListIkan: false
	  });
	}

	onInputNumeric (text) {
		text = text.replace(/[^0-9]/g, '')
		return text.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
	}

	changeToNumeric(text) {
		text = text.replace(/[^0-9]/g, '')
		return Number(text)
	}
	
	convertDateToTimestamp(){
		let arrayDate = this.state.date.split("-")
		let arrayHours = arrayDate[2].split(":")
		var date = new Date(arrayDate[0],arrayDate[1],arrayDate[2].substring(0,2),arrayHours[0].slice(-2),arrayHours[1],0 ,0)
		timestampDate = date.getTime() + ""
		let time = "" + timestampDate.substring(0, 10)
		return time
	}
	
	validateForm(){
		pengolahan = this.state.checked1 || this.state.checked2 || this.state.checked3 || this.state.checked4 || this.state.checked5 || this.state.checked6; 
		console.log(this.state.imageSource1)
		let strValid = "";
		if (this.state.title == '') {
			strValid = "Isi judul lapak \n";
		} else if (this.state.jenisIkan == '') {
			strValid = "Isi jenis ikan \n";
		} else if (this.state.namaIkan == '') {
			strValid = "Isi nama ikan \n";
		} else if(this.state.jenisIkan == 'olahan' && !pengolahan) {
			strValid = "Isi jenis pengolahan ikan \n";
		} else if (this.state.weight == '') {
			strValid = "Isi berat ikan \n";
		} else if (this.state.price == ''){
			strValid = "Isi harga yang diinginkan \n";
		} else if (this.state.pembayaran == ''){
			strValid = "Isi metode pembayaran \n";
		}
		return strValid
	}
}

const styles = StyleSheet.create({
	pickerListIkan: {
	  marginTop: 15,
	  marginBottom: 15
	},
	 pickerTextListIkan: {
	  fontSize: 15,
	  fontFamily:'sans-serif-light',
	  color: '#666666'
	},
	overlayStyleListIkan:{
	  position: 'absolute',
	  top: 0,
	  left: 0,
	  right: 0,
	  bottom: 0,
	  backgroundColor: '#333333',
	  justifyContent: 'center',
	  alignItems: 'center'
	},
	listContainerStyleListIkan: {
	  flex: 1,
	  width: width * 0.8,
	  maxHeight: height * 0.7,
	  backgroundColor: '#fff',
	  borderRadius: 10,
	  marginBottom: 15,
	  overflow: 'hidden'
	},
	 optionTextStyleListIkan: {
	  fontSize:15, 
	  fontFamily:"Montserrat-Regular"
	},
	filterTextInputStyleListIkan: {
	  paddingVertical: 10,
	  paddingHorizontal: 15,
	  flex: 0,
	  height: 50,
	  fontFamily: "Montserrat-Regular",
	  fontSize:15
	},
	titleTextStyleListIkan: {
	  fontFamily:"Helvetica-Bold", 
	  color:"white", 
	  fontSize:20,
	  flex: 0,
	  marginBottom: 15
	},
	container: {
		flex: 1,
		flexDirection: "column",
		justifyContent: "center",
		backgroundColor: "#f0f8ff"
	},
	divider: {
		height: 40,
		alignItems: "center",
		justifyContent: "center",
		left: 0,
		right: 0,
		backgroundColor: "#D3D3D3",
		zIndex: 10
	},
	content: {
		marginTop: 5,
		marginBottom: 5,
		paddingLeft : 10,
		paddingRight : 10
	},
	contentText: {
		fontSize: 18,
		fontFamily: "Montserrat-Regular"
	},
	
	textDivider: {
		fontSize: 24,
		fontWeight: "bold",
	},
	textHeader: {
		fontSize: 36,
		color: "white",
		fontWeight: "bold",
		textAlign: "left",
		alignSelf: "stretch"
	},
	textFooter: {
		fontSize: 20,
		color: "white",
		fontFamily: "Helvetica-Bold"
		
	},
	imageContainer: {
		flex: 1
	},
	image: {
		width: 100,
		height: 100,
	},
	footer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#1e90ff'
	  },
	buttonFooter: {
		alignItems: 'center',
		padding: 10
	  },
	picker2: {
		flex: 1,
		flexDirection: 'row'
	},
	alertUIText: {
    fontSize:15, 
    fontFamily:"Montserrat-Regular"
  },
});
