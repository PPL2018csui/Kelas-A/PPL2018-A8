import React from "react";
import { Text, View, StyleSheet} from "react-native";

export default class Beranda extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<Text>Beranda!</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1, 
		justifyContent: "center", 
		alignItems: "center"
	}
});