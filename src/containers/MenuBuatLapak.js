import { AppRegistry} from "react-native";
import { TabNavigator , TabBarTop} from "react-navigation";
import React from "react";
import BuatLapakJual from "./BuatLapakJual";
import BuatLapakBeli from "./BuatLapakBeli";

export default TabNavigator (
	{
		BuatLapakJual: { 
			screen: BuatLapakJual,
			navigationOptions: {
				title: "Jual"
			},
		},
		BuatLapakBeli: { 
			screen: BuatLapakBeli,
			navigationOptions: {
				title: "Beli"
			},
		}
	},
	{	
		tabBarComponent: TabBarTop,
		tabBarPosition: "top",
		tabBarOptions: {
			activeTintColor: "#666666",
			inactiveTintColor: '#333333',
			showLabel : true,
			labelStyle: {
				fontSize: 16,
				margin:2,
				fontFamily: "Montserrat-Bold"
			},
			allowFontScaling : true,
			indicatorStyle: { 
				backgroundColor: "#666666",
				height: 2},
			style: {backgroundColor: "#D3D3D3", height: 40, padding:0, margin:0},
		},
		animationEnabled: false,
		swipeEnabled: true,
	}
);

AppRegistry.registerComponent("PPLA8kedua", () => TabNavigator);
