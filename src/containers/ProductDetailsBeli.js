import React, { Component } from 'react';
import { AppRegistry, View, Text, Image, ScrollView, TouchableHighlight, StyleSheet, Alert } from 'react-native';
import { headerDetailsStyles, productInfoStyles, productDescStyles, sellerInfoStylesheet } from '../assets/stylesheet/productDetailsStyle';
import { StackNavigator } from 'react-navigation';
import { HeaderDetails, ProductInfo, ProductDesc, SellerInfo } from '../components/ComponentsBeli';
import Pesan from "./Pesan";
import { Icon } from 'react-native-elements';
import Profil from './Profil';
import Edit from './EditProdukJual';

import firebase from "../firebase.js";
var DeviceInfo = require("react-native-device-info");
const uniqueId = DeviceInfo.getUniqueID();

export default class ProductDetails extends Component {
  constructor(props) {
    super(props);
    this.prodId = '';
    this.userId = '';
    this.state = {
      detail: {
        jenisProduk: '',
        ukuran: '',
        weight: '',
        kondisiIkan: '',
        description: '',
      },
      username: '',
      sameId: false,
    }
  }

  static navigationOptions = {
    title: 'Detail produk',
    tabBarVisible: false,
    headerStyle: {
      backgroundColor: '#1e90ff',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: "normal",
      fontSize: 20,
      fontFamily: 'Helvetica-Bold'
    },
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const product = params ? params.product : '';
    this.productRef = firebase.database().ref('productDetails/' + product.productId);
    this.productRef.on('value', (snap) => {
      this.setState({
        detail: snap.val(),
      });
      this.userId = snap.val().userId;
      this.userRef = firebase.database().ref('users/' + this.userId);
      this.userRef.on('value', (snaps) => {
        this.setState({
          username: snaps.val().nama,
        })
      });
      this.setState({totalHarga : ((Number(snap.val().weight)*Number(product.price) + "").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."))})
    });
    
  }

  renderChatButton() {
    if(this.state.sameId == true){

    } else {
      if(this.userId == uniqueId){
        this.setState({sameId: true});
      }
    }

    if (this.state.sameId) {
        return null;
    } else {
        return (
        <View stlye={sellerInfoStylesheet.button}>
          <TouchableHighlight
              style={sellerInfoStylesheet.chatButton}
              underlayColor="#D3D3D3"
							onPress={() => {
								this.props.navigation.navigate("RuangPesan", {
									friendId: this.userId,
								});
							}}
          >
              <Text style={sellerInfoStylesheet.buttonText}>
                  Kirim Pesan
              </Text>
          </TouchableHighlight>
        </View>
        );
    }
  }

  render() {
    const { params } = this.props.navigation.state;
    const product = params ? params.product : '';
    return (
      <View style={{ flex: 1 }}>
        <View style={{
          backgroundColor: '#1e90ff',
          alignItems: 'center',
          flexDirection: 'row',
          flex: 0.1,
        }}>
          <View style={{ flex: 0.15 }}>
            <TouchableHighlight
              onPress={() => {
                this.props.navigation.goBack(null);
              }}
            >
              <Icon
                name='md-arrow-back'
                type='ionicon'
                color='white'
                size={24}
              />
            </TouchableHighlight>
          </View>
          <Text style={{
            flex: 0.65,
            padding: 15,
            fontSize: 20,
            fontFamily: 'Helvetica-Bold',
            color: 'white',
          }}>
            Detail Produk
          </Text>
          {/* <TouchableHighlight style={searchBarStylesheet.subContainer}
            onPress={() => {
              this.props.navigation.navigate('Profile');
            }}
          >
            <View style={searchBarStylesheet.mainContainer}>
              <View style={searchBarStylesheet.container}>
                <Icon
                  name='md-search'
                  type='ionicon'
                  color='#999999'
                  size={15}
                />
              </View>
              <Text style={searchBarStylesheet.content}>
                Search
		                	</Text>
            </View>
          </TouchableHighlight> */}
        </View>
        <ScrollView style={{ flex: 1, backgroundColor: '#F0F8FF' }}>
          <HeaderDetails
            imgSource={product.imageURL}
            title={product.judul}
            price={product.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}
            location={product.location}
            timestamp={product.timestamp}
            satuan={product.satuanIkan}
          />
          <ProductInfo
            weight={this.state.detail.weight}
            kondisi={this.state.detail.kondisiIkan}
            satuan={product.satuanIkan}
            pembayaran={this.state.detail.pembayaran}
            namaIkan={product.namaIkan}
            totalHarga={this.state.totalHarga}
          />
          <View style={sellerInfoStylesheet.mainContainer}>
            <Text style={sellerInfoStylesheet.subTitle}>
              Penjual
                </Text>
            <View style={sellerInfoStylesheet.section}>
              <View style={sellerInfoStylesheet.separator}
              />
            </View>
            <View style={sellerInfoStylesheet.details}>
              <View style={sellerInfoStylesheet.space}>
                <Icon
                  name='user'
                  type='feather'
                  color='#999999'
                  size={24}
                />
              </View>
              <TouchableHighlight style={sellerInfoStylesheet.box}
                onPress={() => {
                  this.props.navigation.navigate('Profil', {
                    id: product.userId,
                    username: this.state.username,
                  });
                }}
              >
                <View>
                  <Text style={sellerInfoStylesheet.content}>
                    {this.state.username}
                  </Text>
                </View>
              </TouchableHighlight>
              {this.renderChatButton()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
};
