import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";

const redCircle = [
	{image: require("../assets/images/red_circle.png")},
];

const PesanItem = ({ pesan, index }) => {
	return (
		<View noDefaultStyles={true}>
			<View style={styles.pesan_item}>
				<View style={styles.pesan_photo}>
					<Image source={pesan.image} style={styles.photo} />
				</View>
				<View style={styles.pesan_text}>
					<View style={styles.text_container}>
						<Text style={styles.pengirim}>{pesan.name}</Text>
						<Text>{pesan.message_content.length <= 40 ? pesan.message_content : pesan.message_content.substring(0,41) + "...."}</Text>
					</View>
				</View>
				{/* <View style={pesan.read ? styles.hide : styles.pesan_notif}>
					<Image source={redCircle} style={styles.pesan_notif} />
				</View> */}
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	pesan_item: {
		flex: 1,
		flexDirection: "row",
		padding:5,
		borderBottomWidth: 0.5,
		borderBottomColor: "#E4E4E4",
		maxHeight: 78
	},
	pesan_text: {
		flex: 2,
		flexDirection: "row",
		margin: 5
	},
	text_container: {
		flex: 2
	},
	pengirim: {
		fontSize: 16,
		fontWeight: "bold",
		color: "#000",
		fontFamily: "montserrat"
	},
	pesan_photo: {
		flex: 0,
		justifyContent: "flex-start"
	},
	photo: {
		justifyContent: "flex-start",
		alignItems: "center",
		width: 50,
		height: 50,
		borderRadius: 25,
	},
	pesan_notif: {
		flex: 0,
		width: 20,
		height: 20,
		borderRadius: 10,
		backgroundColor: "#FF0033",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center"
	},
	hide: {
		width:0,
		height:0
	}
});

export default PesanItem;